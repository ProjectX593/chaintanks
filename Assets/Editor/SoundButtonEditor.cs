﻿using UnityEngine;
using UnityEditor;
using UnityEditor.UI;

[CustomEditor(typeof(SoundButton))]
[CanEditMultipleObjects]
public class SoundButtonEditor : ButtonEditor {

	SerializedProperty clickSoundProp;

	protected override void OnEnable() {
		base.OnEnable();
		clickSoundProp = serializedObject.FindProperty("clickSound");
	}

	public override void OnInspectorGUI() {
		base.OnInspectorGUI();
		SoundButton sb = (SoundButton)target;
		EditorGUILayout.PropertyField(clickSoundProp, new GUIContent("Click Sound"));
		serializedObject.ApplyModifiedProperties();
	}
}
