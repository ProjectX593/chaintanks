﻿using UnityEngine;
using System.Collections;

public class KillOffscreen : MonoBehaviour {

	private const float SAFE_ZONE = 3.0f;

	// Update is called once per frame
	void Update () {
        Vector2 tr = Camera.main.ViewportToWorldPoint(new Vector3(1, 1));
        Vector2 bl = Camera.main.ViewportToWorldPoint(Vector3.zero);
		
        if(transform.position.x > tr.x + SAFE_ZONE ||
            transform.position.x < bl.x  - SAFE_ZONE ||
            transform.position.y > tr.y + SAFE_ZONE ||
            transform.position.y < bl.y - SAFE_ZONE) {
            Destroy(gameObject);
        }
	}
}
