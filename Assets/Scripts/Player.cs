﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;

[Serializable]
public class PlayerData : VehicleData {
	public List<VehicleData> tailData;

	public PlayerData(int upgradeInd, int turretIndex, List<VehicleData> tailData, int health) : base(turretIndex, upgradeInd, health) {
		this.tailData = tailData;
		this.health = health;
	}

	public int GetScrapTotal() {
		int total = 0;
		for(int i = 0; i < tailData.Count; i++) {
			total += tailData[i].upgradeIndex + 1;
		}
		return total;
	}
}

public class Player : Truck {
	
    public int scrapHealAmount;
	
	private CameraManager mCameraManager;
	private int mTurretIndex = -1;

	private const float ANGULAR_TOLERANCE = 1.0f;
	private float mOriginalSpeed;
	
	protected override void Start() {
		mTurretIndex = ProgressionManager.Get().lastPlayerData.turretIndex;
        base.Start();
		mCameraManager = GameObject.Find("CameraManager").GetComponent<CameraManager>();
		InitFromData(ProgressionManager.Get().lastPlayerData);
		mOriginalSpeed = topSpeed;
		UpdateSpeed();
	}
    protected override void FixedUpdate() {
		bool missionComplete = ProgressionManager.Get().missionComplete;

		if (Options.alternateControlModeEnabled) {
			Vector2 intendedDirection = (new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"))).normalized;
			float angularDifference = Globals.NormalizeAngle(intendedDirection.GetDegrees() - 90 - GetComponent<Rigidbody2D>().rotation);

			mLeftPressed = mRightPressed = false;
			mForwardPressed = intendedDirection.magnitude > 0.1 && !missionComplete;
			if (mForwardPressed) {
				mForwardPressed = true;
				if (angularDifference > ANGULAR_TOLERANCE) {
					mLeftPressed = true;
				} else if (angularDifference < -ANGULAR_TOLERANCE) {
					mRightPressed = true;
				}
			}
		} 
		else {
			mForwardPressed = !missionComplete && Input.GetAxis("Vertical") > 0;
			mLeftPressed = !missionComplete && Input.GetAxis("Horizontal") < 0;
			mRightPressed = !missionComplete && Input.GetAxis("Horizontal") > 0;
		}

        base.FixedUpdate();
    }

    protected override void Update() {
		Vehicle curr = this;
		int tailCount = 0;
        while (curr != null) {
			for(var i = 0; i < curr.mTurrets.Count; i++) {
				curr.mTurrets[i].AimAt(Camera.main.ScreenToWorldPoint(Input.mousePosition));
				if (Input.GetMouseButtonDown(0)) {
					curr.mTurrets[i].SetFiring(true);
				} else if (Input.GetMouseButtonUp(0)) {
					curr.mTurrets[i].SetFiring(false);
				}
			}

            curr = curr.mTail;
			tailCount++;
        }

		base.Update();
    }

	protected override void OnTriggerEnter2D(Collider2D other) {
		base.OnTriggerEnter2D(other);

		Vehicle v = other.GetComponent<Vehicle>();
		if(v != null) {
			ReconnectToVehicle(v);
		}
	}

	public void ReconnectToVehicle(Vehicle v) {
		if (!v.disabled || !v.isPlayerOwned) {
			return;
		}

		Vehicle last = this;
		while (last.mTail != null) {
			last = last.mTail;
		}

		Vehicle deadHead = v;
		while (deadHead.mParent != null) {
			deadHead = deadHead.mParent;
		}

		last.mTail = deadHead;
		deadHead.mParent = last;

		do {
			deadHead.SetDisabled(false);
			deadHead = deadHead.mTail;
		} while (deadHead != null);

		UpdateSpeed();
	}

	protected override void OnDie() {
		Globals.SetCursor(true);
		ProgressionManager.Get().KillSavedGame();
		SceneManager.LoadScene("dead");
	}

	public void ApplyHealing(int healAmount) {
		int healPerVehicle = Mathf.Max(healAmount / 6, 1);

		Vehicle curr = this;
		List<Vehicle> sortedVehicles = new List<Vehicle>();
		sortedVehicles.Add(this);

		while(curr.mTail != null) {
			float healthPercent = curr.mTail.health / curr.mTail.GetMaxHealth();
			bool added = false;
			for(int i = 0; i < sortedVehicles.Count; i++) {
				if(healthPercent < sortedVehicles[i].health / sortedVehicles[i].GetMaxHealth()) {
					sortedVehicles.Insert(i, curr.mTail);
					added = true;
					break;
				}
			}
			if (!added) {
				sortedVehicles.Add(curr.mTail);
			}
			curr = curr.mTail;
		}
		
		while(healAmount > 0) {
			bool didHeal = false;
			for(int i = 0; i < sortedVehicles.Count; i++) {
				int toHeal = Mathf.Min(healAmount, healPerVehicle);
				int healed = sortedVehicles[i].Heal(toHeal);
				didHeal = didHeal || healed > 0;
				healAmount -= healed;
			}

			if(!didHeal) {
				return;
			}
		}
	}
	
	public new PlayerData GetData(){
		List<VehicleData> tailData = new List<VehicleData>();
		Vehicle current = mTail;
		while(current != null) {
			tailData.Add(current.GetData());
			current = current.mTail;
		}
		return new PlayerData(mUpgradeIndex, mTurretIndex, tailData, mHealth);
	}

	public void InitFromData(PlayerData data) {
		if(mUpgradeIndex > 0 || mTail != null) {
			print("ERROR: TRIED TO INSTANTIATE A PLAYER OBJECT THAT HAS UPGRADES");
			return;
		}

		AddTurretWithIndex(data.turretIndex);
		for(int i = 0; i < data.upgradeIndex; i++) {
			Upgrade();
		}
		mHealth = data.health;
		InitHealthBar();

		Vehicle current = this;
		for(int i = 0; i < data.tailData.Count; i++) {
			current.AddTail();
			current = current.mTail;
			current.InitFromData(data.tailData[i]);
		}
	}

	public static float CalculateSpeed(float baseSpeed, int numberOfTails) {
		return baseSpeed - (MAX_TAILS - numberOfTails) * baseSpeed * 0.05f;
	}

	public void UpdateSpeed() {
		int tailCount = 0;
		Vehicle current = this;
		while(current.mTail != null) {
			tailCount++;
			current = current.mTail;
		}
		topSpeed = CalculateSpeed(mOriginalSpeed, tailCount);
	}

	public override float GetTopSpeed() {
		return topSpeed * ProgressionManager.Get().GetTechBonusPercent(TechType.speed);
	}

	public override void TakeDamage(int damage) {
		base.TakeDamage(damage);
	}
}
