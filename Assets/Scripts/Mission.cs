﻿using System;

// Only used for grouping missions elsewhere. The missions themselves don't actually care about this.
public enum MissionType {
	safe,
	risky,
	defensive
}

[Serializable]
public class Mission {

	public string sceneId;
	public int time;
	public int minThreat;
	public int maxThreat;
	public int difficulty;
	public float rewardScale;
	public string missionName;

	public Mission(string sceneId, int time, int minThreat, int maxThreat, int difficulty, float rewardScale, string name) {
		this.sceneId = sceneId;
		this.time = time;
		this.minThreat = minThreat;
		this.maxThreat = maxThreat;
		this.difficulty = difficulty;
		this.rewardScale = rewardScale;
		missionName = name;
	}

	public int GetReward(int timeLeft) {
		// The first hour rewards twice as much scrap as any further hours - rewards short missions more, but they have the risk of needing more repairs
		var oneHourReward = 200 + (difficulty + (48 - timeLeft * 2)) * 5;
		return (int)((oneHourReward + (oneHourReward / 2) * (time - 1)) * rewardScale);
	}
}
