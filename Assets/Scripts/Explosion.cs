﻿using UnityEngine;

public class Explosion : MonoBehaviour {

	public GameObject tankExplosion;

	private int mExplosionsLeft = 0;
	private float mTimeUntilNextExplosion = 0.3f;
	private float mExplosionWidth = 0;
	private float mExplosionScale = 1.0f;

	private const float EXPLOSION_TIMING = 0.03f;

	// Use this for initialization
	void Start () {
		mExplosionsLeft = Random.Range(12, 18);
		CreateExplosion(transform.position);
		mExplosionScale = 0.55f;
	}
	
	// Update is called once per frame
	void Update () {
		mTimeUntilNextExplosion -= Time.deltaTime;
		while(mExplosionsLeft > 0 && mTimeUntilNextExplosion <= 0) {
			Vector2 pos = new Vector2(0, Random.Range(mExplosionWidth * 0.17f, mExplosionWidth * 0.45f)).Rotate(Random.Range(0.0f, 359.0f)) + new Vector2(transform.position.x, transform.position.y);
			CreateExplosion(pos);
			mTimeUntilNextExplosion += EXPLOSION_TIMING;
			mExplosionsLeft--;
		}
	}

	public void SetWidth(float width) {
		mExplosionWidth = width;
	}

	private void CreateExplosion(Vector3 position) {
		GameObject explosion = Instantiate(tankExplosion, position, Quaternion.identity) as GameObject;
		float scale = (mExplosionWidth * mExplosionScale) / explosion.GetComponent<SpriteRenderer>().bounds.size.x;
		explosion.transform.localScale = new Vector3(scale, scale, 1.0f);
		mExplosionScale *= 0.9f;
	}
}
