﻿using UnityEngine;
using System.Collections.Generic;

public class Turret : MonoBehaviour {

	public int cost;
	public float fireCooldown;
	public Bullet bullet;
	public float bulletStartDistance;
    public GameObject upgradedTurret;
	public List<AudioClip> fireSounds;
	public Sprite colorblindSprite;

	private Vector2 mAimPosition = new Vector2(3.0f, 3.0f);
	private bool mFiring = false;
	private float mTimeUntilFire = 0.0f;
	private SoundManager mSoundManager;
	private GameObject mPlayer;
	public bool mIsPlayerOwned = true;
	private Sprite mOriginalSprite;
	
	private const float ANGULAR_TOLERANCE = 1.0f;
	private const float FIRE_MAX_RANDOM_DELAY = 0.4f;

	void Start() {
		mSoundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
		mPlayer = GameObject.Find("Player1");
		
		mOriginalSprite = mOriginalSprite == null ? GetComponent<SpriteRenderer>().sprite : mOriginalSprite;
		UpdateColorblindSprite();
	}

	public void AimAt(Vector2 pos) {
		mAimPosition = pos;
	}

	public void Update() {
		Vector2 aimVec = new Vector3(mAimPosition.x, mAimPosition.y) - transform.position;
		transform.rotation = Quaternion.AngleAxis(aimVec.GetDegrees() - 90, Vector3.forward);

		mTimeUntilFire -= Time.deltaTime;
		if (mFiring && !ProgressionManager.Get().missionComplete) {
			if(mTimeUntilFire <= 0) {
				mTimeUntilFire += GetFireCooldown();
				Vector3 bulletDir = transform.rotation * new Vector3(0, bulletStartDistance);
				Bullet b = Instantiate(bullet, transform.position + bulletDir, transform.rotation) as Bullet;
				b.SetPlayerOwned(mIsPlayerOwned);
				b.SetSoundManager(mSoundManager);

				if (mSoundManager.fireSoundsEnabled) {
					mSoundManager.PlayRandomSound(fireSounds, (transform.position - mPlayer.transform.position).magnitude, true);
				}
			}
		}
	}

	public void SetFiring(bool firing) {
		if (!mFiring) {
			mTimeUntilFire = Mathf.Max(Random.Range(0, FIRE_MAX_RANDOM_DELAY), mTimeUntilFire);
		}
		mFiring = firing;
	}

	public void Upgrade() {
		if(upgradedTurret != null) {
			Turret t = upgradedTurret.GetComponent<Turret>();
			fireCooldown = t.fireCooldown;
			bullet = t.bullet;
			bulletStartDistance = t.bulletStartDistance;
			colorblindSprite = t.colorblindSprite;
			mOriginalSprite = t.GetComponent<SpriteRenderer>().sprite;
			upgradedTurret = t.upgradedTurret;
		}

		UpdateColorblindSprite();
	}

	public void SetPlayerOwned(bool playerOwned) {
		mIsPlayerOwned = playerOwned;
	}

	public float GetFireCooldown() {
		if (mIsPlayerOwned) {
			return fireCooldown * 1.0f / ProgressionManager.Get().GetTechBonusPercent(TechType.rateOfFire);
		} else {
			return fireCooldown;
		}
	}

	public void UpdateColorblindSprite() {
		// We can't have this happen before Start() is called
		if (mSoundManager == null || mPlayer == null) {
			return;
		}

		SpriteRenderer sr = GetComponent<SpriteRenderer>();
		if (Options.colorblindEnabled && colorblindSprite != null) {
			sr.sprite = colorblindSprite;
		} else if(mOriginalSprite != null){
			sr.sprite = mOriginalSprite;
		}
	}
}
