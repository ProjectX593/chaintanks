﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/*
 * Tutorial steps:
 * 0 - Intro dialog
 * 1 - Add vehicle
 * 2 - Remove vehicle
 * 3 - Upgrade vehicle
 * 4 - Downgrade vehicle
 * 5 - View turrets
 * 6 - Change turret
 * 7 - Open tech dialog
 * 8 - DONE, allow player to advance
 */

public class UpgradeScreen : MonoBehaviour {
	public Button techButton;
	public Button backButton;
	public VehiclePanel[] vehiclePanels;
	public GameObject techDialog;
	public Text scrapCounter;
	public Text hpText;
	public Text dpsText;
	public Text speedText;
	public Turret[] baseTurrets;
	public GameObject[] tutorialDialogs;
	public GameObject skipTutorialButton;

	private PlayerData mPlayerData;
	private int mTutorialStep = -1;

	void Start() {
		mPlayerData = ProgressionManager.Get().lastPlayerData;

		vehiclePanels[0].LoadVehicle(mPlayerData, true);
		if (mPlayerData.tailData != null) {
			for (var i = 0; i < mPlayerData.tailData.Count; i++) {
				vehiclePanels[i + 1].LoadVehicle(mPlayerData.tailData[i], false);
			}
			if (mPlayerData.tailData.Count < Vehicle.MAX_TAILS) {
				vehiclePanels[mPlayerData.tailData.Count + 1].SetCanAddVehicle(true);
			}
		} else {
			vehiclePanels[1].SetCanAddVehicle(true);
		}

		UpdateScrapCounter();
		UpdateChainStats();

		if (ProgressionManager.Get().tutorialActive) {
			mTutorialStep = 0;
			tutorialDialogs[0].SetActive(true);
			skipTutorialButton.SetActive(true);
		}
	}

	public void OnBackButton() {
		if (mTutorialStep < 0 || (mTutorialStep >= 9 && ProgressionManager.Get().scrap < 50)) {
			SceneManager.LoadScene("missionSelect");
			ProgressionManager.Get().Save();
		}
	}

	public void AddVehicle() {
		ProgressionManager pm = ProgressionManager.Get();
		if (mPlayerData.tailData.Count >= Vehicle.MAX_TAILS || pm.scrap < baseTurrets[0].cost) {
			return;
		}

		mPlayerData.tailData.Add(new VehicleData(0, 0, 9999));
		vehiclePanels[mPlayerData.tailData.Count].LoadVehicle(mPlayerData.tailData[mPlayerData.tailData.Count - 1], false);
		if (mPlayerData.tailData.Count < Vehicle.MAX_TAILS) {
			vehiclePanels[mPlayerData.tailData.Count + 1].SetCanAddVehicle(true);
		}

		pm.RemoveScrap(baseTurrets[0].cost);
		UpdateAllVehicles();

		if(mTutorialStep == 1) {
			IncrementTutorialStep();
		}
	}

	public void UpgradeVehicle(int index) {
		ProgressionManager pm = ProgressionManager.Get();
		Turret oldTurret = vehiclePanels[index].GetTurret();
		int cost = oldTurret.upgradedTurret == null ? 0 : oldTurret.upgradedTurret.GetComponent<Turret>().cost - oldTurret.cost;
		if(cost > pm.scrap) {
			return;
		}

		if (index == 0 && mPlayerData.upgradeIndex < Vehicle.MAX_UPGRADE_TIER) {
			mPlayerData.upgradeIndex++;
			Vehicle v = vehiclePanels[index].GetVehicle();
			mPlayerData.health = v.GetMaxHealth();
		} else if (index > 0 && index <= mPlayerData.tailData.Count && mPlayerData.tailData[index - 1].upgradeIndex < Vehicle.MAX_UPGRADE_TIER) {
			mPlayerData.tailData[index - 1].upgradeIndex++;
			Vehicle v = vehiclePanels[index].GetVehicle();
			mPlayerData.tailData[index - 1].health = v.GetMaxHealth();
		}

		pm.RemoveScrap(cost);
		UpdateScrapCounter();

		if (mTutorialStep == 3 && index == 0) {
			IncrementTutorialStep();
		}
	}

	public void DowngradeVehicle(int index) {
		int price = vehiclePanels[index].GetTurret().cost;

		if (index == 0) {
			if (mPlayerData.upgradeIndex > 0) {
				mPlayerData.upgradeIndex--;
				Vehicle v = vehiclePanels[index].GetVehicle();
				mPlayerData.health = v.GetMaxHealth();

				price -= vehiclePanels[index].GetTurret().cost;
				ProgressionManager.Get().AddScrap(price);

				if(mTutorialStep == 4) {
					IncrementTutorialStep();
				}
			}
		} else if (index > 0 && index <= mPlayerData.tailData.Count) {
			if (mPlayerData.tailData[index - 1].upgradeIndex > 0) {
				mPlayerData.tailData[index - 1].upgradeIndex--;
				Vehicle v = vehiclePanels[index].GetVehicle();
				mPlayerData.tailData[index - 1].health = v.GetMaxHealth();

				price -= vehiclePanels[index].GetTurret().cost;
				ProgressionManager.Get().AddScrap(price);
			} else if (index == mPlayerData.tailData.Count) {
				mPlayerData.tailData.Remove(mPlayerData.tailData[index - 1]);
				vehiclePanels[index].RemoveVehicle();
				vehiclePanels[index].SetCanAddVehicle(true);
				ProgressionManager.Get().AddScrap(price);
				if (index < Vehicle.MAX_TAILS) {
					vehiclePanels[index + 1].SetCanAddVehicle(false);
				}

				if(mTutorialStep == 2 && index == 1) {
					IncrementTutorialStep();
				}
			}
		}

		UpdateAllVehicles();
	}

	public void SetTurretIndex(int vehicleIndex, int turretIndex) {
		int origTurretIndex = vehicleIndex == 0 ? mPlayerData.turretIndex : mPlayerData.tailData[vehicleIndex - 1].turretIndex;
		int scrapChange = vehiclePanels[vehicleIndex].GetTurret().cost;

		if(vehicleIndex == 0) {
			mPlayerData.turretIndex = turretIndex;
		} else if (vehicleIndex - 1 < mPlayerData.tailData.Count){
			mPlayerData.tailData[vehicleIndex - 1].turretIndex = turretIndex;
		}

		scrapChange -= vehiclePanels[vehicleIndex].GetTurret().cost;

		ProgressionManager pm = ProgressionManager.Get();
		if(scrapChange < 0 && pm.scrap < -scrapChange) {
			// Not enough scrap? change it back
			if(vehicleIndex == 0) {
				mPlayerData.turretIndex = origTurretIndex;
			} else {
				mPlayerData.tailData[vehicleIndex - 1].turretIndex = origTurretIndex;
			}
			return;
		}

		pm.AddScrap(scrapChange);
		UpdateScrapCounter();

		if(mTutorialStep == 6 && vehicleIndex == 0) {
			IncrementTutorialStep();
		}
	}

	public void UpdateScrapCounter() {
		scrapCounter.text = "SCRAP: " + ProgressionManager.Get().scrap;
	}

	public void UpdateAllVehicles() {
		for(int i = 0; i < vehiclePanels.Length; i++) {
			vehiclePanels[i].UpdateVehicle();
			vehiclePanels[i].UpdateHealth();
		}

		UpdateChainStats();
		UpdateScrapCounter();
	}

	public void ToggleTechDialog() {
		if(mTutorialStep == 8 && !techDialog.activeSelf) {
			IncrementTutorialStep();
		}

		techDialog.SetActive(!techDialog.activeSelf);
	}

	public void UpdateChainStats() {
		int totalHP = 0;
		float totalDPS = 0;

		var tailCount = 0;
		for(var i = 0; i < vehiclePanels.Length; i++) {
			totalHP += vehiclePanels[i].lastTotalHealth;
			totalDPS += vehiclePanels[i].lastDPS;
			if(i > 0 && vehiclePanels[i].GetVehicle() != null) {
				tailCount++;
			}
		}

		Player player = vehiclePanels[0].basePlayer;
		for(var i = 0; i < mPlayerData.upgradeIndex; i++) {
			player = player.upgradedVehicle.GetComponent<Player>();
		}

		hpText.text = "Total HP: " + totalHP;
		dpsText.text = "Total DPS: " + totalDPS.ToString("F1");
		speedText.text = "Speed: " + Player.CalculateSpeed(player.GetTopSpeed(), tailCount);
	}


	// TUTORIAL CODE
	public void OnIntroOkButton() {
		IncrementTutorialStep();
	}

	public void OnSkipButton() {
		ProgressionManager.Get().TutorialDone(true);
		
		tutorialDialogs[mTutorialStep].SetActive(false);
		skipTutorialButton.SetActive(false);

		mTutorialStep = -1;
	}

	public void IncrementTutorialStep() {
		if (!ProgressionManager.Get().tutorialActive || mTutorialStep < 0) {
			return;
		}

		tutorialDialogs[mTutorialStep].SetActive(false);
		mTutorialStep++;
		if(mTutorialStep < tutorialDialogs.Length) {
			tutorialDialogs[mTutorialStep].SetActive(true);
		} else {
			mTutorialStep = -1;
		}
	}

	public void TutorialTurretClicked() {
		if(mTutorialStep == 5) {
			IncrementTutorialStep();
		}
	}

	public void HealAllVehicles() {
		for(int i = 0; i < vehiclePanels.Length; i++) {
			if (vehiclePanels[i].GetVehicle() != null) {
				vehiclePanels[i].GetVehicleData().health = vehiclePanels[i].GetVehicle().GetMaxHealth();
			}
		}
	}

	public bool AllVehiclesFullyHealed() {
		for(int i = 0; i < vehiclePanels.Length; i++) {
			if(vehiclePanels[i].GetVehicle() != null && (vehiclePanels[i].GetVehicleData().health < vehiclePanels[i].GetVehicle().GetMaxHealth())) {
				return false;
			}
		}

		return true;
	}
}
