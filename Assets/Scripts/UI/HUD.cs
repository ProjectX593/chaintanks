﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class HUD : MonoBehaviour {

	public GameObject missionCompleteText;
	public GameObject pauseMenu;
	public GameObject confirmQuitDialog;
	public GameObject optionsDialog;
	public GameObject reconnectTutorial;
	public GameObject missionCompleteTutorial;

	private bool mMissionCompleted;

	void Start () {
		missionCompleteText.SetActive(false);
		mMissionCompleted = false;
	}
	
	public void OnMissionComplete() {
		if (mMissionCompleted) {
			return;
		}
		mMissionCompleted = true;

		missionCompleteText.SetActive(true);
		if (PlayerPrefs.GetString("missionCompleteTutorialShown", "false") == "false") {
			PlayerPrefs.SetString("missionCompleteTutorialShown", "true");
			missionCompleteTutorial.SetActive(true);
		} else {
			Invoke("ShowResults", 5.0f);
		}
	}

	void ShowResults() {
		ProgressionManager.Get().ShowResults();
	}

	void Update() {
		if (Input.GetKeyDown(KeyCode.Escape)) {
			TogglePauseMenu();
		}
	}

	// stops or starts time based on the state of a dialog before dismissing or showing it - just here to eliminate duplicated code
	private void ToggleTime(bool dialogActive) {
		if (dialogActive) {
			Time.timeScale = 1.0f;
			Globals.SetCursor(false);
		} else {
			Time.timeScale = 0.0f;
			Globals.SetCursor(true);
		}
	}

	public void TogglePauseMenu() {
		ToggleTime(pauseMenu.activeSelf);
		pauseMenu.SetActive(!pauseMenu.activeSelf);
	}

	public void ToggleOptionsDialog() {
		optionsDialog.SetActive(!optionsDialog.activeSelf);
	}

	public void ToggleConfirmQuitDialog() {
		confirmQuitDialog.SetActive(!confirmQuitDialog.activeSelf);
	}

	public void OnConfirmQuit() {
		SceneManager.LoadScene("mainmenu");
		Time.timeScale = 1.0f;
	}

	public void ToggleReconnectTutorial() {
		ToggleTime(reconnectTutorial.activeSelf);
		reconnectTutorial.SetActive(!reconnectTutorial.activeSelf);
	}

	public void DismissMissionCompleteTutorial() {
		ProgressionManager.Get().ShowResults();
	}
}
