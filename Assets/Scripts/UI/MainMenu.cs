﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	public GameObject soundManager;
	public GameObject continueButton;
	public GameObject quitButton;
	public GameObject defaultButtons;
	public GameObject difficultyButtons;

	private bool mMusicStarted = false;
	private SoundManager mSoundManagerInstance = null;

	public void Start() {
		GameObject sm = GameObject.Find("SoundManager");
		if (sm == null) {
			sm = Instantiate(soundManager);
			sm.name = "SoundManager";
		}
		mSoundManagerInstance = sm.GetComponent<SoundManager>();

		Options.updateOptionsFromPrefs();
		Globals.SetCursor(true);

		string savedData = PlayerPrefs.GetString("savedGame", "");
		if(savedData == "") {
			continueButton.SetActive(false);
		}

		if (Application.platform == RuntimePlatform.WebGLPlayer) {
			quitButton.SetActive(false);
		}
	}

	public void Update() {
		// Can't play music until the soundmanager is active...
		if (!mMusicStarted) {
			mMusicStarted = true;
			mSoundManagerInstance.GetComponent<SoundManager>().PlaySongsFromList(SoundManager.MENU_MUSIC, true);
		}
	}

	public void ContinueGame() {
		ProgressionManager pm = ProgressionManager.Get();
		pm.Restart(0);
		pm.Load();

		if(pm.threat >= 100) {
			SceneManager.LoadScene("missionSelect");
		} else {
			SceneManager.LoadScene("upgrade");
		}
	}

	public void ShowNewGameButtons() {
		defaultButtons.SetActive(false);
		difficultyButtons.SetActive(true);
	}

	public void HideNewGameButtons() {
		defaultButtons.SetActive(true);
		difficultyButtons.SetActive(false);
	}

	public void NewGame(int difficulty) {
		ProgressionManager.Get().KillSavedGame();
		StartNewGame(difficulty);
	}

	private void StartNewGame(int difficulty) {
		ProgressionManager pm = ProgressionManager.Get();
		pm.Restart(difficulty);

		SceneManager.LoadScene("upgrade");
	}

	public void Tutorial() {
		SceneManager.LoadScene("tutorial");
	}

	public void OnOptionsButton() {
		SceneManager.LoadScene("options");
	}

	public void OnCreditsButton() {
		SceneManager.LoadScene("credits");
	}

	public void Quit() {
		Application.Quit();
	}
}
