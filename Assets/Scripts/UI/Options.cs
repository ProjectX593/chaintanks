﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Options : MonoBehaviour {
	public static bool alternateControlModeEnabled = false;
	public static bool colorblindEnabled;

	public Button modeButton1;
	public Button modeButton2;
	public Slider soundSlider;
	public Toggle fireSoundsToggle;
	public Slider musicSlider;
	public Toggle colorblindToggle;
	public AudioClip colorblindToggleSound;

	private float mSoundVolume;
	private float mMusicVolume;
	private bool mFireSoundsEnabled;
	private SoundManager mSoundManager = null;

	protected void Start() {
		UpdateButtons();

		mSoundVolume = PlayerPrefs.GetFloat("soundVolume", SoundManager.DEFAULT_SOUND_VOLUME);
		mMusicVolume = PlayerPrefs.GetFloat("musicVolume", SoundManager.DEFAULT_MUSIC_VOLUME);
		mFireSoundsEnabled = PlayerPrefs.GetString("fireSounds", "false") == "true";

		soundSlider.value = mSoundVolume;
		fireSoundsToggle.isOn = mFireSoundsEnabled;
		musicSlider.value = mMusicVolume;
		colorblindToggle.isOn = colorblindEnabled;

		GameObject soundManager = GameObject.Find("SoundManager");
		if (soundManager != null) {
			mSoundManager = soundManager.GetComponent<SoundManager>();
		}
	}

	public static void updateOptionsFromPrefs() {
		alternateControlModeEnabled = PlayerPrefs.GetString("controlScheme", "default") == "alternate";
		colorblindEnabled = PlayerPrefs.GetString("colorblindMode") == "true";
	}

	public void MainMenu() {
		SceneManager.LoadScene("mainmenu");
	}

	public void CommitChanges() {
		PlayerPrefs.SetFloat("soundVolume", mSoundVolume);
		PlayerPrefs.SetFloat("musicVolume", mMusicVolume);
		PlayerPrefs.SetString("fireSounds", mFireSoundsEnabled ? "true" : "false");
		PlayerPrefs.SetString("controlScheme", alternateControlModeEnabled ? "alternate" : "default");
		PlayerPrefs.SetString("colorblindMode", colorblindEnabled ? "true" : "false");

		if (mSoundManager != null) {
			SoundManager sm = mSoundManager.GetComponent<SoundManager>();
			sm.SetSoundVolume(mSoundVolume);
			sm.SetMusicVolume(mMusicVolume);
		}
	}

	public void OnButton1Clicked() {
		alternateControlModeEnabled = false;
		UpdateButtons();
	}

	public void OnButton2Clicked() {
		alternateControlModeEnabled = true;
		UpdateButtons();
	}

	public void OnResetTutorialButton() {
		ProgressionManager.Get().ResetTutorial();
		PlayerPrefs.SetString("tutorialDone", "false");
		PlayerPrefs.SetString("reconnectTutorialShown", "false");
		PlayerPrefs.SetString("missionCompleteTutorialShown", "false");
	}

	public void OnColorblindToggled() {
		// mSoundManager isn't initialized until after the colorblind toggle's initial value is set, this prevents an extra click sound
		if (mSoundManager != null) {
			mSoundManager.PlaySound(colorblindToggleSound, 0f, false);
		}
		colorblindEnabled = colorblindToggle.isOn;

		GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
		for(int i = 0; i < enemies.Length; i++) {
			enemies[i].GetComponent<Vehicle>().UpdateColorblindSprite();
		}
	}

	private void UpdateButtons() {
		modeButton1.interactable = alternateControlModeEnabled;
		modeButton2.interactable = !alternateControlModeEnabled;
	}

	public void OnSoundVolumeChanged() {
		mSoundVolume = soundSlider.value;
	}

	public void OnFireSoundsToggled() {
		mFireSoundsEnabled = fireSoundsToggle.isOn;
		if(mSoundManager != null) {
			mSoundManager.PlaySound(colorblindToggleSound, 0f, false);
			mSoundManager.fireSoundsEnabled = mFireSoundsEnabled;
		}
	}

	public void OnMusicVolumeChanged() {
		mMusicVolume = musicSlider.value;
		if (mSoundManager != null) {
			mSoundManager.SetMusicVolume(mMusicVolume);
		}
	}
}
