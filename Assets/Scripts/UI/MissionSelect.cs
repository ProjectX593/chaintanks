﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

/* Tutorial steps
0) Explain what missions are
1) Reward
2) Time required
3) Threat explanation
4) Mission threat
5) Difficulty
6) Click first mission
7) Threat bar
8) Retaliation Chance
9) Time left
10) Enter mission
 */

public class MissionSelect : MonoBehaviour {

	public Button safeButton;
	public Button riskyButton;
	public Button defensiveButton;
	public Text titleText;
	public Text timeLeftText;
	public Image threatBar;
	public Image potentialThreatBar;
	public Text retaliationChanceText;
	public Text retaliationFillerText;
	public Text finalBattleDescText;
	public Sprite greenThreatImage;
	public Sprite redThreatImage;
	public Sprite greenThreatBar;
	public Sprite redThreatBar;
	public Texture2D crosshairCursor;
	public List<GameObject> tutorialDialogs;
	public GameObject upgradeButton;
	public GameObject skipTutorialButton;
	public GameObject statusBar;

	private List<Mission> mMissions = new List<Mission>();
	private static int mSelectedMissionIndex = -1;
	private int mTutorialStep = -1;

	void Start () {
		ProgressionManager pm = ProgressionManager.Get();
		mMissions = pm.availableMissions;

		if (pm.timeRemaining <= 0 || pm.threat >= 100) {
			safeButton.gameObject.SetActive(false);
			riskyButton.gameObject.SetActive(false);
			defensiveButton.gameObject.SetActive(false);
			statusBar.SetActive(pm.threat >= 100);
		} else {
			InitButton(safeButton.gameObject, mMissions[(int)MissionType.safe]);
			InitButton(riskyButton.gameObject, mMissions[(int)MissionType.risky]);
			InitButton(defensiveButton.gameObject, mMissions[(int)MissionType.defensive]);
		}
		
		if(pm.threat >= 100) {
			titleText.text = "ATTACK IMMINENT";
			upgradeButton.SetActive(false);
		} else if(pm.timeRemaining <= 0) {
			titleText.text = "FINAL BATTLE IMMINENT";

		} else {
			titleText.text = "SELECT MISSION";
		}
		
		timeLeftText.text = pm.timeRemaining + (pm.timeRemaining == 1 ? " HOUR" : " HOURS") + " REMAINING";
		threatBar.transform.localScale = new Vector3(pm.threat / 100.0f, 1, 1);
		potentialThreatBar.transform.localScale = new Vector3(0, 1, 1);
		retaliationChanceText.text = "";
		retaliationFillerText.enabled = false;
		finalBattleDescText.gameObject.SetActive(pm.timeRemaining <= 0);

		if (pm.tutorialActive) {
			mTutorialStep = 0;
			tutorialDialogs[0].SetActive(true);
			skipTutorialButton.SetActive(true);
		}

		if(mSelectedMissionIndex == 0) {
			OnSafeButton();
		} else if(mSelectedMissionIndex == 1) {
			OnRiskyButton();
		} else if(mSelectedMissionIndex == 2) {
			OnDefensiveButton();
		}
	}

	void InitButton(GameObject button, Mission mission) {
		button.transform.Find("Name").GetComponent<Text>().text = mission.missionName;
		button.transform.Find("Reward").GetComponent<Text>().text = mission.GetReward(ProgressionManager.Get().timeRemaining).ToString() + " SCRAP";
		button.transform.Find("Time").GetComponent<Text>().text = mission.time.ToString() + " HOURS";

		button.transform.Find("MinThreatIcon").GetComponent<Image>().overrideSprite = mission.minThreat <= 0 ? greenThreatImage : redThreatImage;
		button.transform.Find("MaxThreatIcon").GetComponent<Image>().overrideSprite = mission.maxThreat <= 0 ? greenThreatImage : redThreatImage;

		Image minBar = button.transform.Find("MinThreatBar").transform.Find("Fill").GetComponent<Image>();
		minBar.overrideSprite = mission.minThreat <= 0 ? greenThreatBar : redThreatBar;
		minBar.transform.localScale = new Vector3(Mathf.Abs(mission.minThreat) / 100.0f, 1, 1);

		Image maxBar = button.transform.Find("MaxThreatBar").transform.Find("Fill").GetComponent<Image>();
		maxBar.overrideSprite = mission.maxThreat <= 0 ? greenThreatBar : redThreatBar;
		maxBar.transform.localScale = new Vector3(Mathf.Abs(mission.maxThreat) / 100.0f, 1, 1);

		button.transform.Find("DifficultyBar").transform.Find("DifficultyFill").transform.localScale = new Vector3(mission.difficulty / 50.0f, 1, 1);
	}

	void EnableButtons() {
		safeButton.interactable = true;
		riskyButton.interactable = true;
		defensiveButton.interactable = true;
	}

	public void OnSafeButton() {
		if(mTutorialStep >= 0 && mTutorialStep != 6) {
			return;
		}

		EnableButtons();
		safeButton.interactable = false;
		mSelectedMissionIndex = 0;
		UpdateRetaliationChance();
		NextTutorialStep();
	}

	public void OnRiskyButton() {
		if (mTutorialStep >= 0) {
			return;
		}

		EnableButtons();
		riskyButton.interactable = false;
		mSelectedMissionIndex = 1;
		UpdateRetaliationChance();
	}

	public void OnDefensiveButton() {
		if (mTutorialStep >= 0) {
			return;
		}
		EnableButtons();
		defensiveButton.interactable = false;
		mSelectedMissionIndex = 2;
		UpdateRetaliationChance();
	}

	private void UpdateRetaliationChance() {
		int threat = ProgressionManager.Get().threat;
		Mission mission = mMissions[mSelectedMissionIndex];
		int range = mission.maxThreat - mission.minThreat;
		int over = Mathf.Max(0, threat + mission.maxThreat - 99);
		float chance = over > 0 ? 1 : 0;
		if (range > 0) {
			chance = Mathf.Min(1, (float)over / range);
		}
		retaliationChanceText.text = (int)(100 * chance) + "%";
		retaliationFillerText.enabled = true;

		potentialThreatBar.transform.localScale = new Vector3(Mathf.Min(1.0f, (threat + Mathf.Max(mission.maxThreat, 0)) / 100.0f), 1, 1);
	}

	public void OnEngageButton() {
		if(mTutorialStep >= 0 && mTutorialStep < 10) {
			return;
		}

		ProgressionManager pm = ProgressionManager.Get();
		SoundManager sm = GameObject.Find("SoundManager").GetComponent<SoundManager>();

		if (pm.threat >= 100) {
			ProgressionManager.Get().StartRetaliationMission();
			sm.PlaySongsFromList(SoundManager.BATTLE_MUSIC);
			SceneManager.LoadScene("retaliationLevel");
		} else if (ProgressionManager.Get().timeRemaining <= 0) {
			pm.StartFinalBoss();
			pm.KillSavedGame();	// kill the save here so the user can't reset the game to try again
			sm.PlaySongsFromList(SoundManager.BOSS_MUSIC);
			SceneManager.LoadScene("bossLevel");
		}
		else if(mSelectedMissionIndex >= 0) {
			string sceneId = mMissions[mSelectedMissionIndex].sceneId;  // startMission changes the mission list so store the ID we're about to use
			pm.StartMission(mMissions[mSelectedMissionIndex]);
			sm.PlaySongsFromList(SoundManager.BATTLE_MUSIC);
			SceneManager.LoadScene(sceneId);
			mSelectedMissionIndex = -1;
		}

		ProgressionManager.Get().TutorialDone();
		Globals.SetCursor(false);
	}

	public void OnUpgradeButton() {
		if(mTutorialStep < 0) {
			SceneManager.LoadScene("upgrade");
		}
	}



	// TUTORIAL STUFF
	public void OnSkipTutorial() {
		ProgressionManager.Get().TutorialDone();
		tutorialDialogs[mTutorialStep].SetActive(false);
		skipTutorialButton.SetActive(false);
		mTutorialStep = -1;
	}

	public void NextTutorialStep() {
		if(mTutorialStep < 0) {
			return;
		}

		tutorialDialogs[mTutorialStep].SetActive(false);
		mTutorialStep++;
		if(mTutorialStep < tutorialDialogs.Count) {
			tutorialDialogs[mTutorialStep].SetActive(true);
		}
	}
}
