﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ResultsScreen : MonoBehaviour {
	
	public Text rewardText;
	public Text powerupScrapText;
	public Text timeLeftText;
	public Text threatText;
	public Text doneButtonText;

	private SoundManager mSoundManager;

	void Start() {
		mSoundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
		mSoundManager.PlaySongsFromList(SoundManager.MENU_MUSIC);
		ProgressionManager pm = ProgressionManager.Get();
		
		rewardText.text = "MISSION REWARD: " + pm.lastScrapReward;
		powerupScrapText.text = "BONUS SCRAP: " + pm.scrapFromPowerups;
		timeLeftText.text = pm.timeRemaining <= 0 ? "YOU ARE OUT OF TIME" :	"YOU HAVE " + pm.timeRemaining + " HOURS LEFT";

		if(pm.threat < 100) {
			threatText.text = pm.timeRemaining <= 0 ? "" : "THREAT: " + pm.threat + " / 100";
		} else {
			threatText.text = "WARNING! ATTACK IMMINENT!";
			doneButtonText.text = "ENGAGE";
		}
	}

	public void OnDoneButton() {
		if (ProgressionManager.Get().threat < 100) {
			SceneManager.LoadScene("upgrade");
		} else {
			ProgressionManager.Get().StartRetaliationMission();
			Globals.SetCursor(false);
			mSoundManager.PlaySongsFromList(SoundManager.BATTLE_MUSIC);
			SceneManager.LoadScene("retaliationLevel");
		}
	}
}
