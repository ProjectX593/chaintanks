﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Credits : MonoBehaviour {

	public void OnIncompetechClick() {
		OpenLink("http://www.incompetech.com");
	}

	public void OnTwitterClick() {

		OpenLink("http://www.twitter.com/projectx593");
	}

	public void OnFacebookClick() {
		OpenLink("https://www.facebook.com/projectx593");
	}

	public void OnTwitchClick() {
		OpenLink("https://www.twitch.tv/projectx593");
	}

	private void OpenLink(string url) {
		if(Application.platform == RuntimePlatform.WebGLPlayer) {
			Application.ExternalEval("window.open(\"" + url + "\")");
		} else {
			Application.OpenURL(url);
		}
	}

	public void MainMenu() {
		SceneManager.LoadScene("mainmenu");
	}
}
