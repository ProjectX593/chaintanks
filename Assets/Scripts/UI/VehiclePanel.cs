﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class VehiclePanel : MonoBehaviour {

	public int vehicleIndex;
	public UpgradeScreen upgradeScreen;
	public Button vehicleButton;
	public Button upgradeButton;
	public Button downgradeButton;
	public Button healButton;
	public Button healAllButton;
	public GameObject vehicleAddSymbol;
	public GameObject vehicleSwapSymbol;
	public Image vehicle;
	public Image turret;
	public GameObject healArea;
	public Image healthBar;
	public Text healthBarText;
	public GameObject turretArea;
	public GameObject[] turrets;
	public GameObject statsArea;
	public Text dmgText;
	public Text dpsText;
	public Text rofText;
	public Text bspdText;
	public Player basePlayer;
	public Vehicle baseTail;
	public Turret[] baseTurrets;

	private VehicleData mVehicleData;
	private bool mIsEmpty = true;
	private bool mCanAddVehicle = false;
	private bool mIsHead = false;

	private const float TURRET_PIXEL_SCALE = 75.0f / 82.0f;  // a bit of a hack, the ratio of the vehicle image size to it's texture size
	private const int SCRAP_PER_HP_HEALED = 2;

	// these are used to save us from recalculating if the UpgradeScreen wants these values
	private int mLastTotalHealth = 0;
	private float mLastDPS = 0;

	void Awake() {
		SetElementsActive(false);
		vehicleAddSymbol.SetActive(false);
		healButton.transform.Find("AmountText").GetComponent<Text>().text = "Heal " + GetHealButtonAmount();
	}

	private int GetHealButtonAmount() {
		// we multiply this amount by the health multiplier, then later divide it... It's a little bit hacky, but the
		return 5 * ProgressionManager.Get().GetDifficultyHealthMultiplier();
	}

	public void OnUpgradeButton() {
		if(mVehicleData.health < GetVehicle().GetMaxHealth()) {
			return;
		}

		upgradeScreen.UpgradeVehicle(vehicleIndex);
		UpdateVehicle();
		UpdateTurrets();
		UpdateHealth();
		upgradeScreen.UpdateChainStats();
	}

	public void OnDowngradeButton() {
		if (mVehicleData.health < GetVehicle().GetMaxHealth()) {
			return;
		}

		upgradeScreen.DowngradeVehicle(vehicleIndex);
		UpdateVehicle();
		UpdateTurrets();
		UpdateHealth();
		upgradeScreen.UpdateChainStats();
	}

	public void Heal(int amount = -1) {
		ProgressionManager pm = ProgressionManager.Get();
		int multiplier = ProgressionManager.Get().GetDifficultyHealthMultiplier();
		int healed = Mathf.Min(amount < 0 ? 99999 : amount, (pm.scrap * multiplier) / SCRAP_PER_HP_HEALED, GetVehicle().GetMaxHealth() - mVehicleData.health);
		mVehicleData.health += healed;
		pm.RemoveScrap((healed * SCRAP_PER_HP_HEALED) / multiplier);
		UpdateHealth();
		upgradeScreen.UpdateScrapCounter();
	}

	public void OnHealButton() {
		Heal(GetHealButtonAmount());
	}

	public void OnHealAllButton() {
		Heal();
	}

	public void OnVehicleClick() {
		if (!mIsEmpty) {
			bool showHealth = turretArea.activeSelf;
			turretArea.SetActive(!showHealth);
			healArea.SetActive(showHealth);

			if (showHealth) {
				UpdateHealth();
			} else { 
				UpdateTurrets();
				upgradeScreen.TutorialTurretClicked();
			}
		}
		if (mIsEmpty && mCanAddVehicle) {
			upgradeScreen.AddVehicle();
		}
	}

	public void OnDeleteClick() {

	}

	public void OnTurretClick(int index) {
		upgradeScreen.SetTurretIndex(vehicleIndex, index);
		UpdateVehicle();
		OnVehicleClick();
		upgradeScreen.UpdateChainStats();
	}

	public VehicleData GetVehicleData() {
		return mVehicleData;
	}

	public Vehicle GetVehicle() {
		if(mIsEmpty) {
			return null;
		}
		Vehicle v = mIsHead ? basePlayer : baseTail;
		for (int i = 0; i < mVehicleData.upgradeIndex; i++) {
			v = v.upgradedVehicle.GetComponent<Vehicle>();
		}

		return v;
	}

	// passing -1 will use the turret that this vehicle is equipped with
	public Turret GetTurret(int index = -1) {
		if(mIsEmpty) {
			return null;
		}

		Turret t = baseTurrets[index < 0 ? mVehicleData.turretIndex : index];
		for (int i = 0; i < mVehicleData.upgradeIndex; i++) {
			t = t.upgradedTurret.GetComponent<Turret>();
		}

		return t;
	}

	public void LoadVehicle(VehicleData data, bool isHead) {
		mIsEmpty = false;
		SetCanAddVehicle(false);
		mVehicleData = data;
		mIsHead = isHead;
		
		UpdateVehicle();
		
		SetElementsActive(true);
		turretArea.SetActive(false);
		UpdateHealth();
	}

	private void CapVehicleHealth() {
		Vehicle v = GetVehicle();
		if (mVehicleData.health > v.GetMaxHealth()) {
			mVehicleData.health = v.GetMaxHealth();
		}
	}

	public void RemoveVehicle() {
		if (mIsEmpty) {
			return;
		}
		mIsEmpty = true;
		SetElementsActive(false);
		mLastDPS = 0;
	}

	private void SetElementsActive(bool active) {
		vehicle.gameObject.SetActive(active);
		vehicleSwapSymbol.SetActive(active);
		turret.gameObject.SetActive(active);
		upgradeButton.gameObject.SetActive(active);
		downgradeButton.gameObject.SetActive(active);
		healArea.SetActive(active);
		turretArea.SetActive(active);
		statsArea.SetActive(active);
	}

	public void SetCanAddVehicle(bool canAddVehicle) {
		vehicleAddSymbol.SetActive(canAddVehicle);
		mCanAddVehicle = canAddVehicle;
	}

	public void UpdateVehicle() {
		if (mIsEmpty) {
			return;
		}

		Vehicle v = GetVehicle();
		Turret t = GetTurret();
		
		vehicle.overrideSprite = v.GetComponent<SpriteRenderer>().sprite;
		Sprite turretSprite = t.GetComponent<SpriteRenderer>().sprite;
		turret.overrideSprite = turretSprite;
		turret.rectTransform.sizeDelta = new Vector2(turretSprite.rect.width * TURRET_PIXEL_SCALE, turretSprite.rect.height * TURRET_PIXEL_SCALE);

		UpdateUpgradeButtons();
		UpdateStats();
		CapVehicleHealth();
	}

	private void UpdateUpgradeButtons() {
		Text upText = upgradeButton.transform.Find("CostText").GetComponent<Text>();
		Turret t = GetTurret();

		if (mVehicleData.upgradeIndex == Vehicle.MAX_UPGRADE_TIER) {
			upText.text = "MAX";
		} else {
			upText.text = (t.upgradedTurret.GetComponent<Turret>().cost - t.cost).ToString();
		}
		
		Text downText = downgradeButton.transform.Find("CostText").GetComponent<Text>();

		if (vehicleIndex == 0 && mVehicleData.upgradeIndex == 0) {
			downText.text = "---";
		}
		else if (mVehicleData.upgradeIndex == 0) {
			downText.text = "+" + t.cost;
		} else {
			Turret downgradedTurret = baseTurrets[mVehicleData.turretIndex];
			for (var i = 0; i < mVehicleData.upgradeIndex - 1; i++) {
				downgradedTurret = downgradedTurret.upgradedTurret.GetComponent<Turret>();
			}
			downText.text = "+" + (t.cost - downgradedTurret.cost);
		}
	}

	public void UpdateHealth() {
		Vehicle v = GetVehicle();
		if(v == null) {
			return;
		}
		healthBarText.text = mVehicleData.health + "/" + v.GetMaxHealth();
		mLastTotalHealth = v.GetMaxHealth();
		healthBar.transform.localScale = new Vector3(Mathf.Min(1, (float)mVehicleData.health / v.GetMaxHealth()), 1);

		int multiplier = ProgressionManager.Get().GetDifficultyHealthMultiplier();
		int maxHeal = (v.GetMaxHealth() - mVehicleData.health) / multiplier;
		string maxHealCost = Mathf.Max(0, (maxHeal * SCRAP_PER_HP_HEALED)).ToString();
		healButton.transform.Find("BottomLayout").Find("ScrapText").GetComponent<Text>().text = maxHeal >= GetHealButtonAmount() * multiplier ? "10" : maxHealCost;
		healAllButton.transform.Find("BottomLayout").Find("ScrapText").GetComponent<Text>().text = maxHealCost;

		Color c = new Color(1f, 1f, 1f, mVehicleData.health < v.GetMaxHealth() ? 0.5f : 1f);
		upgradeButton.GetComponent<Image>().color = c;
		downgradeButton.GetComponent<Image>().color = c;
		upgradeButton.GetComponent<Button>().interactable = mVehicleData.health >= v.GetMaxHealth();
		downgradeButton.GetComponent<Button>().interactable = mVehicleData.health >= v.GetMaxHealth();
	}

	public void UpdateTurrets() {
		if (mIsEmpty) {
			return;
		}

		Turret equippedTurret = GetTurret();
		for(int i = 0; i < turrets.Length; i++) {
			Turret t = GetTurret(i);
			Image turretImage = turrets[i].transform.Find("TurretImage").GetComponent<Image>();
			Sprite turretSprite = t.GetComponent<SpriteRenderer>().sprite;
			turretImage.overrideSprite = turretSprite;
			turretImage.rectTransform.sizeDelta = new Vector2(turretSprite.rect.width * TURRET_PIXEL_SCALE, turretSprite.rect.height * TURRET_PIXEL_SCALE);

			Text turretText = turrets[i].transform.Find("CostText").Find("TurretText").GetComponent<Text>();
			if (i < mVehicleData.turretIndex) {
				turretText.text = "+" + (equippedTurret.cost - t.cost);
			} else if (i == mVehicleData.turretIndex) {
				turretText.text = "---";
			} else {
				turretText.text = (t.cost - equippedTurret.cost).ToString();
			}
		}
	}

	private void UpdateStats() {
		Turret t = GetTurret();
		mLastDPS = t.bullet.damage / t.GetFireCooldown();

		dmgText.text = "DMG: " + t.bullet.damage;
		dpsText.text = "DPS: " + (mLastDPS).ToString("N1");
		rofText.text = "ROF: " + (60.0f / t.GetFireCooldown()).ToString("F1") + " RPM";
		bspdText.text = "BULLET SPD: " + t.bullet.GetSpeed().ToString("F1");
	}

	public int lastTotalHealth {
		get {
			return mLastTotalHealth;
		}
	}

	public float lastDPS {
		get {
			return mLastDPS;
		}
	}
}
