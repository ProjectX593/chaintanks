﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DeadMenu : MonoBehaviour {
	public Text totalScrapText;

	public void Start() {
		GameObject.Find("SoundManager").GetComponent<SoundManager>().PlaySongsFromList(SoundManager.MENU_MUSIC);
		if (totalScrapText != null) {
			totalScrapText.text = "TOTAL SCRAP COLLECTED: " + ProgressionManager.Get().totalScrapCollected;
		}
	}

	public void MainMenu() {
		SceneManager.LoadScene("mainmenu");
	}
}
