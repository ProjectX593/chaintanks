﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SoundButton : Button {

	public AudioClip clickSound;
	public int testValue;
	
	public override void OnPointerClick(PointerEventData eventData) {
		base.OnPointerClick(eventData);
		if (clickSound != null) {
			GameObject.Find("SoundManager").GetComponent<SoundManager>().PlaySound(clickSound, 0f, false);
		}
	}
}