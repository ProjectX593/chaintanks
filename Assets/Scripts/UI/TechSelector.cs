﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TechSelector : MonoBehaviour {

	public string techName;
	public UpgradeScreen upgradeScreen;
	public TechType techType;
	public Sprite icon;
	public Image iconImage;
	public Text nameText;
	public delegate void SetTechLevelCallback();
	public GameObject costArea;
	public Text costText;
	public GameObject healWarningText;

	private int mTechLevel = -1;

	public void Start() {
		iconImage.sprite = icon;
		SetTechLevel(ProgressionManager.Get().GetTechLevel(techType), true);
	}

	public void OnEnable() {
		SetTechLevel(ProgressionManager.Get().GetTechLevel(techType), true, true);
	}

	private int GetCost(int techLevel) {
		return Tech.techCosts[techLevel] - Tech.techCosts[mTechLevel];
	}

	public void SetTechLevel(int techLevel, bool free = false, bool force = false) {
		// we need to force it when re-enabling since unity sets the animations techLevel property to 0
		if (mTechLevel == techLevel && !force) {	
			return;
		}

		ProgressionManager pm = ProgressionManager.Get();
		if(!free && techLevel < mTechLevel) {
			pm.AddScrap(Tech.techCosts[mTechLevel] - Tech.techCosts[techLevel]);
		} else if(!free && techLevel > mTechLevel) {
			int cost = Tech.techCosts[techLevel] - Tech.techCosts[mTechLevel];
			if(cost > pm.scrap) {
				return;
			} else {
				pm.RemoveScrap(cost);
			}
		}

		mTechLevel = techLevel;
		GetComponent<Animator>().SetInteger("techLevel", techLevel);
		nameText.text = techName + ":\n+" + (mTechLevel * Tech.GetBonusForType(techType) * 100f) + "% " + Tech.GetPostfixForType(techType);

		pm.SetTechLevel(techType, techLevel);
		if (!free && techType == TechType.hp) {
			upgradeScreen.HealAllVehicles();
		}
		upgradeScreen.UpdateAllVehicles();
	}

	// each button will target a different level depending on what level you are now. If you try to click on the level you're already at it'll target one level below you.
	private int GetTargetLevel(int techButtonIndex) {
		if(mTechLevel == techButtonIndex + 1) {
			return techButtonIndex;
		} else {
			return techButtonIndex + 1;
		}
	}

	public void OnTechButton(int techButtonIndex) {
		// don't allow upgrades when not at full hp!
		if (techType == TechType.hp && !upgradeScreen.AllVehiclesFullyHealed()) {
			return;
		}

		SetTechLevel(GetTargetLevel(techButtonIndex));
		OnTechButtonMouseEnter(techButtonIndex);
	}

	public void OnTechButtonMouseEnter(int buttonIndex) {
		nameText.gameObject.SetActive(false);

		if (techType == TechType.hp && !upgradeScreen.AllVehiclesFullyHealed()) {
			healWarningText.SetActive(true);
		} else {
			costArea.SetActive(true);
			costText.text = GetCost(GetTargetLevel(buttonIndex)).ToString();
		}
	}

	public void OnTechButtonMouseExit(int buttonIndex) {
		nameText.gameObject.SetActive(true);
		costArea.SetActive(false);
		if (healWarningText != null) {
			healWarningText.SetActive(false);
		}
	}
}
