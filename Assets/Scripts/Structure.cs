﻿using UnityEngine;
using System.Collections;

public class Structure : Enemy {

	public Sprite destroyedSprite;
	public float dropChance;
	public GameObject powerupDrop;

	private bool mDead = false;

	protected override void Start() {
		base.Start();
		mOrbitDistance = 2.7f;  // this gives the turrets a constant range at which they fire

		foreach (Transform child in transform) {
			Turret t = child.GetComponent<Turret>();
			if (t != null) {
				mTurrets.Add(t);
			}
		}

		int upgrades = (int)ProgressionManager.Get().GetDifficultyValue(0, 6);
		int initialHealth = mHealth;    // health won't scale like vehicles do, but that's fine
		for (var i = 0; i < upgrades; i++) {
			for (var j = 0; j < mTurrets.Count; j++) {
				mTurrets[j].Upgrade();
			}
			mHealth += initialHealth;
			maxHealth += initialHealth;
		}
	}

	protected override void Update() {
		base.Update();
		for (var i = 0; i < mTurrets.Count; i++) {
			mTurrets[i].Update();
		}
	}

	protected override void FixedUpdate() {
		// does nothing
	}

	protected override void UpdateOffscreenArrow() {
		// also does nothing!
	}

	public override void AddRandomTurret() {
		// does nothing, use the turret already on the tower
	}

	protected override void OnDie() {
		mSoundManager.PlayRandomSound(deathSounds, (transform.position - mPlayer.transform.position).magnitude);
		mDead = true;
		Explode();
		Destroy(mHealthBar);
		GetComponent<SpriteRenderer>().sprite = destroyedSprite;
		sAllEnemies.Remove(this);   // don't let enemies avoid dead buildings

		if (powerupDrop != null && Random.value < dropChance) {
			Instantiate(powerupDrop, transform.position, Quaternion.identity);
		}

		for (int i = 0; i < mTurrets.Count; i++) {
			Destroy(mTurrets[i].gameObject);
		}
		mTurrets.Clear();
	}

	public override void TakeDamage(int damage) {
		if (!mDead) {
			base.TakeDamage(damage);
		}
	}
}