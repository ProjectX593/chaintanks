﻿using UnityEngine;
using System.Collections.Generic;

public class Boss : Enemy {

	private const float MAX_FOLLOW_DIST = 6f;
	private const float TAIL_DISTANCE = 4.4f;
	private const int BOSS_TAIL_COUNT = 5;
	private const float MIN_FIRE_DISTANCE = 18;

	private static int sAliveBossCount = 0;

	protected override void Start() {
		base.Start();
		mOrbitDistance = 5.5f;
		sAliveBossCount = BOSS_TAIL_COUNT + 1;

		List<Transform> anchorPositions = new List<Transform>();
		foreach(Transform child in transform) {
			anchorPositions.Add(child);
		}

		foreach(Transform child in anchorPositions) {
			AddRandomTurret();
			Turret t = mTurrets[mTurrets.Count - 1];
			t.transform.position = child.position;
		}

		mHealthBar.transform.localScale = new Vector3(4f, 1.5f, 1f);
		mHealthBar.transform.position = new Vector3(0, 2f, 0);
	}

	protected override void InitChildren() {
		mTotalHealth = maxHealth;
		if (GameObject.FindGameObjectsWithTag("Boss").Length > 1) {
			return;
		}

		Boss current = this;
		for(int i = 0; i < BOSS_TAIL_COUNT; i++) {
			current.AddTail();
			current = current.mTail.GetComponent<Boss>();
		}
	}

	protected override void FixedUpdate() {
		if (mParent == null) {
			base.FixedUpdate();
		} else {
			UpdateFollowPoints();
			FollowParent();
		}
	}
	
	protected override float GetMinFireDistance() {
		return MIN_FIRE_DISTANCE;
	}


	protected override float GetMaxFollowDistance() {
		return MAX_FOLLOW_DIST;
	}

	protected override float GetTailDistance() {
		return TAIL_DISTANCE;
	}

	protected override Vehicle GenerateTail() {
		return Instantiate(gameObject).GetComponent<Vehicle>();
	}

	protected override float GetHealthBarOffset() {
		return 1.6f;
	}

	protected override void OnDie() {
		base.OnDie();
		sAliveBossCount--;

		if(sAliveBossCount <= 0) {
			ProgressionManager.Get().AllEnemiesDead();
		}
	}

	public override bool disabled {
		get {
			return false;
		}
	}
}
