﻿using UnityEngine;

public static class Vector2Extension {

    public static Vector2 Rotate(this Vector2 v, float degrees) {
        float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
        float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

        return new Vector2(cos * v.x - sin * v.y, sin * v.x + cos * v.y);
    }

	public static float DotProduct(this Vector2 v, Vector2 other) {
		return v.x * other.x + v.y * other.y;
	}
	
	public static float Determinant(this Vector2 v, Vector2 other) {
		return v.x * other.y - v.y * other.x;
	}
	
	// These get the angle with respect to the X axis
	public static float GetRadians(this Vector2 vec) {
		return Mathf.Atan2(vec.y, vec.x);
	}

	public static float GetDegrees(this Vector2 vec) {
		return Mathf.Atan2(vec.y, vec.x) * Mathf.Rad2Deg;
	}

	public static Quaternion GetAngle(this Vector2 vec) {
		float angle = vec.GetDegrees();
		return Quaternion.AngleAxis(angle, Vector3.forward);
	}
}