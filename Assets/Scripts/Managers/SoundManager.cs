﻿using UnityEngine;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour {

	public AudioSource musicSource;
	public AudioSource lowPrioritySource;
	public float minPitch;
	public float maxPitch;
	public bool fireSoundsEnabled;
	// would do this as a dictionary, but the unity editor won't allow it
	public List<AudioClip> menuMusic;
	public List<AudioClip> battleMusic;
	public List<AudioClip> bossMusic;

	public const string MENU_MUSIC = "menu";
	public const string BATTLE_MUSIC = "battle";
	public const string BOSS_MUSIC = "boss";
	public const float DEFAULT_SOUND_VOLUME = 0.8f;
	public const float DEFAULT_MUSIC_VOLUME = 0.5f;

	private const float MUSIC_MULTIPLIER_CHANGE_RATE = 0.25f;
	private const float NEXT_SONG_DELAY = 1.2f;
	private const float MAX_SOUND_DISTANCE = 15f;

	private float mSoundVolume;
	private float mMusicVolume;
	private float mMusicVolumeMultiplier = 1;
	private List<AudioClip> mSongList = null;
	private string mSongListName = "";
	private int mLastSongPlayed = -1;
	private bool mSwitchingSongs = false;

	public void Start() {
		mSongListName = "";
		mSwitchingSongs = false;
		mSoundVolume = PlayerPrefs.GetFloat("soundVolume", DEFAULT_SOUND_VOLUME);
		string test = PlayerPrefs.GetString("fireSounds", "false");
		fireSoundsEnabled = PlayerPrefs.GetString("fireSounds", "false") == "true";
		mMusicVolume = PlayerPrefs.GetFloat("musicVolume", DEFAULT_MUSIC_VOLUME);
		GetComponent<AudioSource>().volume = mSoundVolume;
		lowPrioritySource.volume = mSoundVolume;
		musicSource.volume = mMusicVolume;

		DontDestroyOnLoad(gameObject);
	}

	public void Update() {
		if (mSwitchingSongs) {
			mMusicVolumeMultiplier -= Time.deltaTime * (1f / NEXT_SONG_DELAY);
			musicSource.volume = mMusicVolume * mMusicVolumeMultiplier;

			if(mMusicVolumeMultiplier <= 0) {
				mSwitchingSongs = false;
				PlayNextSong();
			}
		}
	}
	
	public void PlaySound(AudioClip clip, float distance, bool randomizePitch = true, bool lowPriority = false) {
		if (distance > MAX_SOUND_DISTANCE) {
			return;
		}

		AudioSource audioSource = lowPriority ? lowPrioritySource : GetComponent<AudioSource>();
		if (randomizePitch) {
			audioSource.pitch = Random.Range(minPitch, maxPitch);
		}
		audioSource.PlayOneShot(clip, (MAX_SOUND_DISTANCE - distance) / MAX_SOUND_DISTANCE);
	}

	public void PlayRandomSound(List<AudioClip> clips, float distance, bool lowPriority = false) {
		AudioClip clip = null;
		if(clips.Count == 1) {
			PlaySound(clips[0], distance, true, lowPriority);
		} else if(clips.Count > 1) {
			PlaySound(clips[Random.Range(0, clips.Count)], distance, true, lowPriority);
		}
	}

	public void PlaySongsFromList(string listName, bool noDelay = false) {
		if(mSongListName == listName) {
			return;
		}
		mSongListName = listName;

		switch (listName) {
			case MENU_MUSIC:
				mSongList = menuMusic;
				break;
			case BATTLE_MUSIC:
				mSongList = battleMusic;
				break;
			case BOSS_MUSIC:
				mSongList = bossMusic;
				break;
			default:
				Debug.Log("ERROR: Tried to play invalid song list - " + listName);
				return;
		}

		mLastSongPlayed = -1;

		if (!mSwitchingSongs) {
			mSwitchingSongs = true;
			StopAllCoroutines();

			if (noDelay) {
				PlayNextSong();
				mSwitchingSongs = false;
			} else {
				StartCoroutine("PlayNextSongCoroutine", (1 / MUSIC_MULTIPLIER_CHANGE_RATE) + NEXT_SONG_DELAY);
			}
		}
	}

	IEnumerator<WaitForSecondsRealtime> PlayNextSongCoroutine(float delay) {
		yield return new WaitForSecondsRealtime(delay);
		PlayNextSong();
	}

	public void PlayNextSong() {
		// Try 10 times to generate a unique song number
		int songNum = 0;
		for(int i = 0; i < 10; i++) {
			songNum = Random.Range(0, mSongList.Count);
			if(songNum != mLastSongPlayed || mSongList.Count == 1) {
				break;
			}
		}

		mLastSongPlayed = songNum;
		mMusicVolumeMultiplier = 1.0f;
		musicSource.volume = mMusicVolume;
		musicSource.Stop();
		musicSource.PlayOneShot(mSongList[songNum]);
		StopAllCoroutines();
		StartCoroutine(PlayNextSongCoroutine(mSongList[songNum].length + NEXT_SONG_DELAY));
	}

	public void SetSoundVolume(float volume) {
		GetComponent<AudioSource>().volume = volume;
		lowPrioritySource.volume = volume;
		mSoundVolume = volume;
	}

	public void SetMusicVolume(float volume) {
		musicSource.volume = volume;
		mMusicVolume = volume;
	}
}
