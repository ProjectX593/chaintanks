﻿using UnityEngine;

public class CameraManager : MonoBehaviour {

	public int targetCameraSizeOverride = 0;
	public int tailCameraAdjustStart;
	public int tailCameraAdjustEnd;
	public float cameraBaseSize;
	public float sizePerExtraTail;
	public float cameraAdjustSpeed;
	public float cameraSizeTolerance;
	public float mouseFollowMaxDist;
	public float mouseFollowPercentPerSec;

	private float mTargetCameraSize;
	private Player mPlayer;
	private Vector2 mLastCameraAdjust = new Vector2();

	void Start() {
		if(targetCameraSizeOverride > 0) {
			mTargetCameraSize = targetCameraSizeOverride;
		}
		mPlayer = GameObject.Find("Player1").GetComponent<Player>();
	}

	void Update() {
		float w = Screen.width / 2.0f;
		float h = Screen.height / 2.0f;
		float max = Mathf.Max(w, h);
		Vector2 cameraAdjust = new Vector2((Input.mousePosition.x - w) / max, (Input.mousePosition.y - h) / max);
		cameraAdjust *= mouseFollowMaxDist;

		Vector2 moveVec = cameraAdjust - mLastCameraAdjust;
		mLastCameraAdjust = mLastCameraAdjust + moveVec.normalized * Time.deltaTime * moveVec.magnitude * mouseFollowPercentPerSec;

		Camera.main.transform.position = new Vector3(mPlayer.transform.position.x + mLastCameraAdjust.x, mPlayer.transform.position.y + mLastCameraAdjust.y, -10);

		// Slowly adjust the camera size to meet the target size
		if (Mathf.Abs(Camera.main.orthographicSize - mTargetCameraSize) > cameraSizeTolerance) {
			if (Camera.main.orthographicSize > mTargetCameraSize) {
				Camera.main.orthographicSize = Mathf.Max(cameraBaseSize, Camera.main.orthographicSize - cameraAdjustSpeed * Time.deltaTime);
			} else {
				Camera.main.orthographicSize = Mathf.Min(cameraBaseSize + (tailCameraAdjustEnd - tailCameraAdjustStart) * sizePerExtraTail,
					Camera.main.orthographicSize + cameraAdjustSpeed * Time.deltaTime);
			}
		}
	}
}
