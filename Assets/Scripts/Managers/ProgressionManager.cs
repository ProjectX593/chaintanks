﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class ProgressionManager {
	// For Serialized Data
	[System.Serializable]
	public class ProgressionData {
		public int timeRemaining;
		public int threat;
		public PlayerData lastSavedPlayer;
		public int scrap = STARTING_SCRAP;
		public List<Mission> availableMissions = new List<Mission>();
		public int[] techLevels = new int[4];
		public int totalScrapCollected;
		public int difficulty;
	}

	static ProgressionManager instance = null;

	private const int STARTING_SCRAP = 250;

	private bool mMissionComplete = false;
	private bool mTutorial = true;
	private int mCurrentDifficulty;
	private int mLastScrapReward = 0;
	private int mScrapFromPowerups = 0;
	private bool mFinalBossStarted = false;
	private int[] mDifficultyHealthMultiplier = { 6, 3, 1 };	// easy, medium, hard
	//Serialized
	private int mTimeRemaining;
	private int mThreat;
	private PlayerData mLastSavedPlayer;
	private int mScrap = STARTING_SCRAP;
	private List<Mission> mAvailableMissions = new List<Mission>();
	private int[] mTechLevels = new int[4];
	private int mTotalScrapCollected;
	private int mDifficultySetting;

	private const int MIN_DIFFICULTY = 0;
	private const int MAX_DIFFICULTY = 100;

	public static ProgressionManager Get() {
		if (instance == null) {
			instance = new ProgressionManager();
		}
		return instance;
	}

	public ProgressionManager() {
		mTutorial = PlayerPrefs.GetString("tutorialDone") != "true";
	}

	private void GenerateMissions() {
		List<Mission> oldMissions = mAvailableMissions;

		MissionDeck deck = MissionDeck.Get();
		mAvailableMissions = new List<Mission>();
		mAvailableMissions.AddRange(oldMissions);
		
		for(int i = 0; i < 3; i++) {
			if (oldMissions.Count == 0) {
				mAvailableMissions.Add(deck.GetRandomCard((MissionType)i));
			} else {
				while (mAvailableMissions[i] == oldMissions[i]) {
					mAvailableMissions[i] = deck.GetRandomCard((MissionType)i);
				}
			}
		}
	}

	public void Restart(int difficulty) {
		mTotalScrapCollected = 0;
		mTimeRemaining = 24;
		mThreat = 0;
		mScrap = STARTING_SCRAP;
		mLastSavedPlayer = new PlayerData(0, 0, new List<VehicleData>(), 9999);
		mLastScrapReward = 0;
		mDifficultySetting = difficulty;
		GenerateMissions();
		mFinalBossStarted = false;
		mMissionComplete = false;

		for (int i = 0; i < (int)TechType.techTypeCount; i++) {
			mTechLevels[i] = 0;
		}
	}

	public void StartMission(Mission mission) {
		mMissionComplete = false;
		mCurrentDifficulty = mission.difficulty + (24 - mTimeRemaining)*2;
		mLastScrapReward = mission.GetReward(mTimeRemaining);
		mScrap += mission.GetReward(mTimeRemaining);
		mTotalScrapCollected += mission.GetReward(mTimeRemaining);
		mTimeRemaining = Mathf.Max(0, mTimeRemaining - mission.time);
		mThreat = Mathf.Clamp(mThreat + Random.Range(mission.minThreat, mission.maxThreat), 0, 100);
		mScrapFromPowerups = 0;
		GenerateMissions();
	}

	public void StartRetaliationMission() {
		StartMission(new Mission("retaliationLevel", 2, -100, -100, 30, 0.3f, "Counterattack!"));
	}

	public void AddPowerupScrap(int scrap) {
		AddScrap(scrap);
		mScrapFromPowerups += scrap;
		mTotalScrapCollected += scrap;
	}

	public void AddScrap(int scrap) {
		mScrap += scrap;
	}

	public void RemoveScrap(int scrap) {
		mScrap -= scrap;
	}

	public int currentDifficulty {
		get {
			return mCurrentDifficulty;
		}
	}

	public void AllEnemiesDead() {
		GameObject.Find("HUD").GetComponent<HUD>().OnMissionComplete();
		mMissionComplete = true;
	}

	public void ShowResults() {
		Player p = GameObject.Find("Player1").GetComponent<Player>();

		GameObject[] heals = GameObject.FindGameObjectsWithTag("HealPowerup");
		for(int i = 0; i < heals.Length; i++) {
			heals[i].GetComponent<HealPowerup>().ApplyEffect(false);
		}

		Globals.SetCursor(true);
		if (mFinalBossStarted) {
			SceneManager.LoadScene("win");
			KillSavedGame();
		} else {
			GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

			for(int i = 0; i < players.Length; i++) {
				if(players[i].GetComponent<Player>() != null) {
					continue;
				}
				p.ReconnectToVehicle(players[i].GetComponent<Vehicle>());
			}

			mLastSavedPlayer = p.GetData();
			SceneManager.LoadScene("missionResults");
			Save();
		}

	}
	
	// gets a value between a specified min and max that depends on difficulty. Used for scaling values with the level's difficulty value
	public float GetDifficultyValue(float min, float max, bool invertDifficulty = false, bool zeroMiddle = false) {
		float difficultyScale = (currentDifficulty - MIN_DIFFICULTY) / (float)(MAX_DIFFICULTY - MIN_DIFFICULTY);
		if (invertDifficulty) {
			difficultyScale = 1 - difficultyScale;
		}
		if (zeroMiddle) {
			if (min > 0 || max < 0) {
				Debug.Log("ERROR: Bad values passed to zero middle difficulty config!");
				return 0;
			}

			if (difficultyScale <= 0.5) {
				return (1 - difficultyScale * 2) * min;
			} else {
				return (difficultyScale - 0.5f) * 2 * max;
			}
		} else {
			return min + (max - min) * difficultyScale;
		}
	}

	public void SetTechLevel(TechType type, int techLevel) {
		mTechLevels[(int)type] = techLevel;
	}

	public int GetTechLevel(TechType type) {
		return mTechLevels[(int)type];
	}

	public float GetTechBonusPercent(TechType type) {
		return 1.0f + Tech.GetBonusForType(type) * GetTechLevel(type);
	}

	public List<Mission> availableMissions {
		get {
			return mAvailableMissions;
		}
	}

	public void UpdatePlayerData(PlayerData data) {
		mLastSavedPlayer = data;
	}

	public void TutorialDone(bool clearDialogs = false) {
		mTutorial = false;
		PlayerPrefs.SetString("tutorialDone", "true");

		if (clearDialogs) {
			PlayerPrefs.SetString("reconnectTutorialShown", "true");
			PlayerPrefs.SetString("missionCompleteTutorialShown", "true");
		}
	}

	public void ResetTutorial() {
		mTutorial = true;
	}

	public void Save() {
		ProgressionData data = new ProgressionData();
		data.timeRemaining = mTimeRemaining;
		data.threat = mThreat;
		data.lastSavedPlayer = mLastSavedPlayer;
		data.scrap = mScrap;
		data.availableMissions = mAvailableMissions;
		data.techLevels = mTechLevels;
		data.difficulty = mDifficultySetting;
		data.totalScrapCollected = mTotalScrapCollected;

		try {
			string saveData = JsonUtility.ToJson(data);
			PlayerPrefs.SetString("savedGame", saveData);
		} catch (System.Exception e) {
			Debug.Log("SAVE EXCEPTION: " + e.Message);
		}
	}

	public void Load() {
		try {
			string saveData = PlayerPrefs.GetString("savedGame", "");
			if(saveData == "") {
				return;
			}
			ProgressionData data = JsonUtility.FromJson<ProgressionData>(saveData);

			mTimeRemaining = data.timeRemaining;
			mThreat = data.threat;
			mLastSavedPlayer = data.lastSavedPlayer;
			mScrap = data.scrap;
			mAvailableMissions = data.availableMissions;
			mTechLevels = data.techLevels;
			mDifficultySetting = data.difficulty;
			mTotalScrapCollected = data.totalScrapCollected;
		} catch (System.Exception e) {
			Debug.Log("LOAD EXCEPTION: " + e.Message);
		}

	}

	public void KillSavedGame() {
		PlayerPrefs.SetString("savedGame", "");
	}

	public void StartFinalBoss() {
		mMissionComplete = false;
		mFinalBossStarted = true;
	}

	public int GetDifficultyHealthMultiplier() {
		return mDifficultyHealthMultiplier[mDifficultySetting];
	}

	public bool tutorialActive {
		get {
			return mTutorial;
		}
	}

	public int timeRemaining {
		get {
			return mTimeRemaining;
		}
	}

	public int lastScrapReward {
		get {
			return mLastScrapReward;
		}
	}

	public PlayerData lastPlayerData {
		get {
			return mLastSavedPlayer;
		}
	}

	public bool missionComplete {
		get {
			return mMissionComplete;
		}
	}

	public int threat {
		get {
			return mThreat;
		}
	}

	public int scrap {
		get {
			return mScrap;
		}
	}

	public int scrapFromPowerups {
		get {
			return mScrapFromPowerups;
		}
	}

	public int totalScrapCollected {
		get {
			return mTotalScrapCollected;
		}
	}
}
