﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class Spawner : MonoBehaviour {

	public GameObject enemy;
	public float spawnDistance;

	private const int MIN_ENEMY_CAP = 2;
	private const int MAX_ENEMY_CAP = 6;
	private const int MIN_ENEMY_SPAWN_COUNT = 6;
	private const int MAX_ENEMY_SPAWN_COUNT = 16;
	private const float MIN_ENEMY_STRENGTH_INCREASE = 0.4f;
	private const float MAX_ENEMY_STRENGTH_INCREASE = 1.0f;
	private const float MIN_SPAWN_DELAY = 8;
	private const float MAX_SPAWN_DELAY = 12;

	// values computed based on difficulty
	private int mEnemyCap;
	private int mEnemiesLeft;
	private float mEnemyStrengthIncrease;
	private float mSpawnDelay;
	private Text mEnemiesLeftText;

	private GameObject mPlayer;
	private float mTimeUntilSpawn = 0;
	private int mEnemyCount = 0;
	private float mEnemyStrength = 0;
	private List<GameObject> mSpawnPoints = new List<GameObject>();

	void Start () {
		mPlayer = GameObject.Find("Player1");
		mEnemiesLeftText = GameObject.Find("enemyCounterText").GetComponent<Text>();

		ProgressionManager pm = ProgressionManager.Get();
		mEnemyCap = (int)pm.GetDifficultyValue(MIN_ENEMY_CAP, MAX_ENEMY_CAP);
		mEnemiesLeft = (int)pm.GetDifficultyValue(MIN_ENEMY_SPAWN_COUNT, MAX_ENEMY_SPAWN_COUNT);
		mEnemyStrengthIncrease = pm.GetDifficultyValue(MIN_ENEMY_STRENGTH_INCREASE, MAX_ENEMY_STRENGTH_INCREASE);
		mSpawnDelay = pm.GetDifficultyValue(MIN_SPAWN_DELAY, MAX_SPAWN_DELAY, true);
		
		mEnemyStrength = (int)pm.GetDifficultyValue(0, (Vehicle.MAX_TAILS + 1) * Vehicle.MAX_UPGRADE_TIER - 1);	// -1 to take into account the fact that the vehicle starts with a free head
		mEnemiesLeftText.text = "Enemies Left: " + mEnemiesLeft;
	
		foreach (Transform child in transform) {
			if(child.tag == "SpawnPoint") {
				mSpawnPoints.Add(child.gameObject);
			}
		}
	}

	void Update () {
		mTimeUntilSpawn -= Mathf.Max(0, Time.deltaTime);

		if(mTimeUntilSpawn > 1.0f && mEnemyCount == 0) {
			mTimeUntilSpawn = 1.0f;
		}

		if (mEnemyCount < mEnemyCap && mTimeUntilSpawn <= 0 && mEnemiesLeft > 0) {
			GameObject spawnPos = null;
			// We only try 10 times to set the pos to something at a valid range, then we give up
			for (int i = 0; i < 10; i++) {
				spawnPos = mSpawnPoints[Random.Range(0, mSpawnPoints.Count)];
				if((spawnPos.transform.position - mPlayer.transform.position).magnitude > spawnDistance) {
					break;
				}
			}
			
			Enemy newEnemy = (Instantiate(enemy, spawnPos.transform.position, Quaternion.AngleAxis(spawnPos.transform.localEulerAngles.z, Vector3.forward)) as GameObject).GetComponent<Enemy>();

			for (int i = 0; i < mEnemyStrength; i++) {
				newEnemy.AwardStrength();
			}

			mEnemyCount++;
			mEnemiesLeft--;
			mTimeUntilSpawn = mSpawnDelay;
			mEnemyStrength += mEnemyStrengthIncrease;
		}

		if(mEnemiesLeft <= 0 && mEnemyCount <= 0) {
			ProgressionManager.Get().AllEnemiesDead();
		}
	}

	public void OnEnemyDie() {
		mEnemyCount--;
		mEnemiesLeftText.text = "Enemies Left: " + (mEnemiesLeft + mEnemyCount);
	}
}
