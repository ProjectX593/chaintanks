﻿using UnityEngine;

public class Globals : MonoBehaviour {
	
	public GameObject playerTail;
	public GameObject enemyTail;
	public GameObject healthBar;
	public GameObject explosion;
	public float despawnDistance;

	// puts any angle in degrees between -180 and 180
	public static float NormalizeAngle(float angle) {
		while (angle < -180) {
			angle += 360;
		}
		while (angle > 180) {
			angle -= 360;
		}
		return angle;
	}

	public static void SetCursor(bool pointer) {
        if (pointer) {
			Cursor.SetCursor(Resources.Load("pointerCursor", typeof(Texture2D)) as Texture2D, new Vector2(0, 0), CursorMode.Auto);
		} else {
			Cursor.SetCursor(Resources.Load("crosshair_cursor", typeof(Texture2D)) as Texture2D, new Vector2(19, 19), CursorMode.Auto);
		}
	}
}
