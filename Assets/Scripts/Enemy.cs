﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Enemy : Truck {
	public int percentChanceToTargetHead;
	public GameObject offscreenEnemyArrow;
	public GameObject healPowerup;
	
    private bool mOrbitsLeft;
	protected float mOrbitDistance;
	private Vehicle mTarget;
	private Camera mMainCamera;
	private GameObject mOffscreenArrow;
	protected int mBufferedStrength = 0;
	private bool mStarted = false;
	private bool mFullyUpgraded = false;
	protected int mTotalHealth = 0;

	private const float MIN_ORBIT_DISTANCE = 1.0f;
	private const float MAX_ORBIT_DISTANCE = 5.5f;
	private const float MIN_FIRE_DISTANCE = 4f;
	private const float ENEMY_REPEL_DISTANCE = 4.0f;
	private const float ENEMY_REPEL_MAGNITUDE = 2.5f;
	private const float HEAL_SCALE = 0.12f;

	protected static List<Enemy> sAllEnemies = new List<Enemy>();

	protected override void Start() {
		if (GetComponent<Structure>() == null) {
			sAllEnemies.Add(this);
		}
        mGlobals = GameObject.Find("Globals").GetComponent<Globals>();
		mMainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();

		mOrbitsLeft = Random.value <= 0.5f;
		mOrbitDistance = Random.Range(MIN_ORBIT_DISTANCE, MAX_ORBIT_DISTANCE);

        base.Start();
		SelectTarget();

		mForwardPressed = true; // they never stop moving... inspiring... and just a little scary
		mStarted = true;

		for (int i = 0; i < mBufferedStrength; i++) {
			AwardStrength();
		}
		InitChildren();

		int actualStrength = 0;
		Vehicle current = this;
		while(current != null) {
			actualStrength += current.upgradeTier + 1;
			current = current.mTail;
		}
	}

	void OnDestroy() {
		sAllEnemies.Remove(this);
	}

	protected virtual void InitChildren() {
		mTotalHealth = 0;
		Vehicle current = this;
		while (current != null) {
			mTotalHealth += current.GetMaxHealth();
			current.AddRandomTurret();
			current = current.mTail;
		}
	}

	public virtual void AwardStrength() {
		if (mFullyUpgraded || !mStarted) {
			mBufferedStrength++;
			return;
		}

		// two rules for upgrades - no tail can be a higher level than the head, the head must not 2 above the lowest tail
		List<Vehicle> eligibleVehicles = new List<Vehicle>();
		eligibleVehicles.Add(this);
		int lowestUpgradeTier = Vehicle.MAX_UPGRADE_TIER;
		int totalStrength = upgradeTier;
		int totalVehicles = 1;
		Vehicle current = this.mTail;
		while(current != null) {
			if(current.upgradeTier < upgradeTier) {
				eligibleVehicles.Add(current);
			}
			if(current.upgradeTier < lowestUpgradeTier) {
				lowestUpgradeTier = current.upgradeTier;
			}
			totalStrength += current.upgradeTier;
			totalVehicles++;
			current = current.mTail;
		}
		
		if (totalStrength >= (Vehicle.MAX_TAILS + 1) * Vehicle.MAX_UPGRADE_TIER) {
			mFullyUpgraded = true;
			return;
		}
		
		if (upgradeTier - lowestUpgradeTier >= 2) {
			eligibleVehicles.RemoveAt(0);
		}
		
		bool upgraded = false;
		float chanceBoost = 0;
		while (!upgraded) {	// really not super efficient... enemies spawning could be quite costly, revisit if I see lag spikes on enemy spawns
			for(int i = 0; i < eligibleVehicles.Count; i++) {
				Vehicle eligible = eligibleVehicles[i];
				if(eligible.upgradedVehicle != null && Random.value < eligible.percentUpgradeChance / 100.0f + chanceBoost) {
					eligible.Upgrade();
					upgraded = true;
					break;
				}
			}

			if (totalVehicles < Vehicle.MAX_TAILS + 1 && !upgraded && Random.value < 0.6 + chanceBoost) {
				current = this;
				while(current.mTail != null) {
					current = current.mTail;
				}
				current.AddTail();
				upgraded = true;
			}
			chanceBoost += 0.1f;
		}
	}

	private Vector2 GetOrbitVector(Vehicle target, float distance, bool orbitsLeft) {
		Vector2 vec = target.transform.position - transform.position;

		if (vec.magnitude < 2 * distance) {
			vec = vec.Rotate((orbitsLeft? 180 : -180) * (distance * 2 - vec.magnitude) / (distance * 2));
		}
		vec.Normalize();
		return vec;
	}

	protected override void FixedUpdate() {
		if (mTarget == null) {
			return;
		}

		Vector2 intendedDirection = GetOrbitVector(mTarget, mOrbitDistance, mOrbitsLeft);
		
		for(int i = 0; i < sAllEnemies.Count; i++) {
			Enemy e = sAllEnemies[i];
			if (e == this) {
				continue;
			}

			float dist = Vector3.Distance(transform.position, e.transform.position);
			if (dist < ENEMY_REPEL_DISTANCE) {
				Vector2 repelVec = transform.position - e.transform.position;
				repelVec.Normalize();
				repelVec.Rotate(45);    // skew it so enemies steer in opposite directions
				repelVec *= ENEMY_REPEL_MAGNITUDE * ((ENEMY_REPEL_DISTANCE - dist) / ENEMY_REPEL_DISTANCE);
				intendedDirection.x += repelVec.x;
				intendedDirection.y += repelVec.y;
			}
		}

		Rigidbody2D rb = GetComponent<Rigidbody2D>();
		float angularDifference = Globals.NormalizeAngle(intendedDirection.GetDegrees() - 90 - rb.rotation);
		
		mRightPressed = angularDifference < -1;
		mLeftPressed = angularDifference > 1;

		base.FixedUpdate();
    }
	
    protected override void Update() {
		if(mPlayer == null) {
			return;
		}

		base.Update();
		if(mTurrets.Count == 0) {
			return;
		}

		SelectTarget();

		//see: http://danikgames.com/blog/how-to-intersect-a-moving-target-in-2d/
		//then: http://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
		Vector2 aimPoint = mTarget.transform.position;

		if (mPlayer.speed > 0.01) {
			float targetSpeed = mPlayer.speed;
			float bulletSpeed = 0f;
			for(int i = 0; i < mTurrets.Count; i++) {
				bulletSpeed += mTurrets[i].bullet.GetSpeed();
			}
			bulletSpeed /= mTurrets.Count;
			
			Vector2 targetMovement = new Vector2(0, targetSpeed);
			targetMovement = targetMovement.Rotate(mTarget.GetComponent<Rigidbody2D>().rotation);
			Vector2 toTarget = (mTarget.transform.position - transform.position).normalized;

			Vector2 targetParallel = toTarget.DotProduct(targetMovement) * toTarget;    // project targetMovement onto toTarget with dot product
			Vector2 interceptPerpendicular = targetMovement - targetParallel;   // could be called targetPerpendicular - they're the same
			float perpMag = interceptPerpendicular.magnitude;
			float e = bulletSpeed * bulletSpeed - perpMag * perpMag;
			if (e >= 0) {
				Vector2 interceptParallel = toTarget * Mathf.Sqrt(e);   // there's a right triangle here - we know the bullet speed and the perp magnitude, get the parallel
				Vector2 intercept = interceptParallel + interceptPerpendicular; // rather than rotate stuff all over the place, add these together to get the intercept

				// We need the point in front of the target to shoot at, even if it's not accurate for anything but the first bullet
				float bottom = targetMovement.Determinant(intercept);
				if (bottom != 0) {
					Vector3 temp = (transform.position - mTarget.transform.position);
					float time = ((new Vector2(temp.x, temp.y)).Determinant(intercept)) / (bottom);
					aimPoint = mTarget.transform.position + (new Vector3(targetMovement.x, targetMovement.y, 0) * time);
				}
			}
		}

		Vehicle curr = this;
		while (curr != null) {
			for(int i = 0; i < curr.mTurrets.Count; i++) {
				Turret t = curr.mTurrets[i];
				t.AimAt(aimPoint);
				float range = (mTarget.transform.position - t.transform.position).magnitude;
				t.SetFiring(range < mOrbitDistance * 2.2f || range < GetMinFireDistance());
			}
			curr = curr.mTail;
		}

		UpdateOffscreenArrow();
    }

	protected virtual float GetMinFireDistance() {
		return MIN_FIRE_DISTANCE;
	}

	protected virtual void UpdateOffscreenArrow() {
		Vector2 center = mMainCamera.transform.position;
		Vector2 bottomLeft = mMainCamera.ScreenToWorldPoint(new Vector3());
		Vector2 topRight = center + (center - bottomLeft);

		float x = transform.position.x;
		float y = transform.position.y;
		if (x > bottomLeft.x && x < topRight.x && y > bottomLeft.y && y < topRight.y) {
			if(mOffscreenArrow != null) {
				Destroy(mOffscreenArrow);
			}
			return;
		}

		if(mOffscreenArrow == null) {
			mOffscreenArrow = Instantiate(offscreenEnemyArrow);
		}
		
		Vector2 pos = new Vector2(x, y);
		Vector2 toTarget = pos - center;
		Vector2 cornerVec = topRight - center;
		Vector2 toTargetAbs = new Vector2(Mathf.Abs(toTarget.x), Mathf.Abs(toTarget.y));
		Vector3 center3 = center;
		float yPos = Mathf.Tan(toTargetAbs.normalized.GetRadians()) * (cornerVec).x;
		if(yPos <= cornerVec.y) {
			float pct = yPos / cornerVec.y * (toTarget.y / Mathf.Abs(toTarget.y));
			mOffscreenArrow.transform.position = center3 + new Vector3(cornerVec.x * 0.9f * toTarget.x / Mathf.Abs(toTarget.x), cornerVec.y * pct * 0.9f, 0);
		} else {
			float xPos = Mathf.Tan(Mathf.PI / 2.0f - toTargetAbs.normalized.GetRadians()) * cornerVec.y;
			float pct = xPos / cornerVec.x * (toTarget.x / Mathf.Abs(toTarget.x));
			mOffscreenArrow.transform.position = center3 + new Vector3(cornerVec.x * pct * 0.9f, cornerVec.y * 0.9f * toTarget.y / Mathf.Abs(toTarget.y), 0);
		}
		mOffscreenArrow.transform.rotation = toTarget.Rotate(-90.0f).GetAngle();
	}

    protected override void OnDie() {
		if(GetComponent<Police>() == null && GetComponent<Escort>() == null) {
			GameObject spawner = GameObject.Find("Spawner");
			if (spawner != null) {
				spawner.GetComponent<Spawner>().OnEnemyDie();
			}

			GameObject heal = Instantiate(healPowerup, transform.position, Quaternion.identity) as GameObject;
			heal.GetComponent<HealPowerup>().SetHealAmount((int)(mTotalHealth * HEAL_SCALE));
		}
		
		if(mOffscreenArrow != null) {
			Destroy(mOffscreenArrow);
		}

		base.OnDie();
    }

    public override void Upgrade() {
		if(upgradedVehicle == null) {
			print("WARNING: TRIED TO UPGRADE UNUPGRADEABLE ENEMY");
			return;
		}
		
		base.Upgrade();
    }

	private void SelectTarget() {
		Vehicle curr = mPlayer.mTail;
		Vehicle closest = mPlayer;

		// Almost never happens but it can...
		if(mPlayer == null) {
			return;
		}

		float closestDist = Vector3.Distance(transform.position, closest.transform.position);

		while(curr != null) {
			float dist = Vector3.Distance(transform.position, curr.transform.position);
			if(dist  < closestDist) {
				closestDist = dist;
				closest = curr;
			}
			curr = curr.mTail;
		}

		mTarget = closest;
	}

	private void SelectNewTarget() {
		if(Random.value < percentChanceToTargetHead / 100.0f) {
			mTarget = mPlayer;
			return;
		}

		int totalSegments = 1;
		Vehicle curr = mPlayer;
		while(curr.mTail != null) {
			totalSegments++;
			curr = curr.mTail;
		}

		float rand = Random.Range(0, totalSegments);
		mTarget = mPlayer;
		while (rand >= 1) {
			rand--;
			Vehicle v = mTarget.GetComponent<Vehicle>();
			if(v == null) {
				break;
			}
			mTarget = v.mTail;
		}
	}
}
