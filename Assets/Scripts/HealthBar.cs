﻿using UnityEngine;
using System.Collections;

// Hackiest health bar ever
public class HealthBar : MonoBehaviour {

	public GameObject fill;

	private float mFillWidth = 0.0f;
	private float mHealthPercent = 1.0f;

	void Start () {
		mFillWidth = fill.GetComponent<SpriteRenderer>().bounds.size.x / transform.localScale.x;
		SetHealthPercent(mHealthPercent);
	}

	public void SetVisible(bool visible) {
		GetComponent<SpriteRenderer>().enabled = visible;
		fill.GetComponent<SpriteRenderer>().enabled = visible;
	}
	
	public void SetHealthPercent(float healthPercent) {
		mHealthPercent = healthPercent;
		if (mFillWidth != 0.0f) {
			fill.transform.localScale = new Vector3(mHealthPercent, 1);
			fill.transform.localPosition = new Vector3((-mFillWidth / 2) * (1.0f - mHealthPercent), 0);
		}
	}

	public void Update() {
	}
}
