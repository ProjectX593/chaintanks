﻿using UnityEngine;
using System.Collections;

public class Powerup : MonoBehaviour {

	public AudioClip pickupSound;

	private void OnTriggerEnter2D(Collider2D other) {
		if(other.GetComponent<Player>() != null) {
			ApplyEffect();
			Destroy(gameObject);
		}
	}

	public virtual void ApplyEffect(bool playSound = true) {
		if (playSound) {
			GameObject.Find("SoundManager").GetComponent<SoundManager>().PlaySound(pickupSound, 0);
		}
	}
}
