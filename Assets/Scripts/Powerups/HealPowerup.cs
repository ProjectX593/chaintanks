﻿using UnityEngine;
using System.Collections;

public class HealPowerup : Powerup {

	private int mHealAmount;

	public override void ApplyEffect(bool playSound = true) {
		base.ApplyEffect(playSound);
		GameObject.Find("Player1").GetComponent<Player>().ApplyHealing(mHealAmount);
	}

	public void SetHealAmount(int healAmount) {
		mHealAmount = healAmount * ProgressionManager.Get().GetDifficultyHealthMultiplier(); ;
	}
}
