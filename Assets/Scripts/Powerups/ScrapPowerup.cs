﻿using UnityEngine;
using System.Collections;

public class ScrapPowerup : Powerup {

	private const int MIN_SCRAP = 50;
	private const int MAX_SCRAP = 175;

	public override void ApplyEffect(bool playSound = true) {
		base.ApplyEffect(playSound);
		ProgressionManager pm = ProgressionManager.Get();
		pm.AddPowerupScrap((int)pm.GetDifficultyValue(MIN_SCRAP, MAX_SCRAP));
	}
}
