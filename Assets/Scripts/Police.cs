﻿using UnityEngine;
using System.Collections;

public class Police : Enemy {

	public float strengthModifier = 1.0f;
	public GameObject policeTail;

	// Use this for initialization
	protected override void Start () {
		base.Start();

		GameObject oldTail = mGlobals.enemyTail;
		mGlobals.enemyTail = policeTail;
		ProgressionManager pm = ProgressionManager.Get();
		// copied from Spawner... if changes are needed eliminate the duplication
		int enemyStrength = (int)(pm.GetDifficultyValue(0, (Vehicle.MAX_TAILS + 1) * Vehicle.MAX_UPGRADE_TIER - 1) * strengthModifier);
		for (int i = 0; i < enemyStrength; i++) {
			AwardStrength();
		}
		mGlobals.enemyTail = oldTail;

		// Was already called once by base, but that was before the strength was added. Safe to call again.
		InitChildren();
	}
}
