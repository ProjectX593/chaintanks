﻿using UnityEngine;
using System.Collections;

public class DisappearAfterTime : MonoBehaviour {
	public float disappearTime;
	public bool destroy = true;

	void Start() {
		StartCoroutine(disappear());
	}

	IEnumerator disappear() {
		yield return new WaitForSeconds(disappearTime);
		if (destroy) {
			Destroy(gameObject);
		} else {
			gameObject.GetComponent<SpriteRenderer>().enabled = false;
		}
	}
}
