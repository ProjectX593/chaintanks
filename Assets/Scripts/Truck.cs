﻿using UnityEngine;
using System.Collections.Generic;

public class Truck : Vehicle {

	public GameObject tailToAdd;
	public float acceleration;
	public float brakingDeceleration;
	public float topSpeed;
	public float rotationRate;
	public float rotationAcceleration;
	public int scrapForNextLevel;

	protected bool mForwardPressed = false;
	protected bool mLeftPressed = false;
	protected bool mRightPressed = false;
	private float currentRotationRate = 0;

	protected float mSpeed;
	private int[] mBlockedSides = new int[4] {0, 0, 0, 0};	// top, right, bottom, left, like CSS. Each # indicates how many walls are blocking from that side so we can touch more than one at a time

	protected override void Start() {
		GetComponent<Rigidbody2D>().freezeRotation = true;
		base.Start();
	}
	
	protected override void FixedUpdate() {
		if (mForwardPressed) {
			mSpeed = Mathf.Min(GetTopSpeed(), mSpeed + acceleration * Time.deltaTime);
		} else {
			mSpeed = Mathf.Max(0, mSpeed - brakingDeceleration * Time.deltaTime);
		}

		Rigidbody2D rb = GetComponent<Rigidbody2D>();
		if(mLeftPressed ^ mRightPressed) {
			float adjust = 0;

			currentRotationRate +=  rotationRate / rotationAcceleration * Time.deltaTime * (mLeftPressed ? 1 : -1);
			currentRotationRate = Mathf.Clamp(currentRotationRate, -rotationRate, rotationRate);

			// Low speeds scale a lot faster than higer speeds
			float rot = currentRotationRate * Time.deltaTime;
			if (mSpeed / GetTopSpeed() < 0.2f) {
				adjust = (mSpeed / GetTopSpeed()) / 0.2f  * 0.6f * rot;
			} else {
				adjust = 0.6f * rot + (mSpeed / GetTopSpeed() - 0.2f) / 0.8f * 0.4f * rot;
			}

			/*if (mLeftPressed) {
				rb.rotation += adjust;
			} else {
				rb.rotation -= adjust;
			}*/
			rb.rotation += adjust;
		}

		Vector2 move = (new Vector2(0, mSpeed * Time.deltaTime)).Rotate(rb.rotation);

		if (mBlockedSides[0] > 0 && move.y > 0) {
			move.y = 0;
		}
		if (mBlockedSides[1] > 0 && move.x > 0) {
			move.x = 0;
		}
		if (mBlockedSides[2] > 0 && move.y < 0) {
			move.y = 0;
		}
		if(mBlockedSides[3] > 0 && move.x < 0) {
			move.x = 0;
		}

		transform.position = transform.position + new Vector3(move.x, move.y, 0);

		base.FixedUpdate();
	}

	public override void Upgrade() {
		if (upgradedVehicle == null) {
			print("WARNING: TRIED TO UPGRADE UNUPGRADEABLE TRUCK");
			return;
		}

		Truck upgrade = upgradedVehicle.GetComponent<Truck>();
		acceleration = upgrade.acceleration;
		brakingDeceleration = upgrade.brakingDeceleration;
		topSpeed = upgrade.topSpeed;
		rotationRate = upgrade.rotationRate;
		base.Upgrade();
	}

	public virtual float GetTopSpeed() {
		return topSpeed;
	}

	public float speed {
		get {
			return mSpeed;
		}
		set {
			mSpeed = value;
		}
	}

	private int GetEdgeDirection(EdgeTile edge) {
		if (edge.horizontal) {
			if (edge.transform.position.y < transform.position.y) {
				return 2;
			} else {
				return 0;
			}
		} else {
			if (edge.transform.position.x < transform.position.x) {
				return 3;
			} else {
				return 1;
			}
		}
	}

	protected override void OnTriggerEnter2D(Collider2D other) {
		base.OnTriggerEnter2D(other);

		EdgeTile edge = other.GetComponent<EdgeTile>();
		if (edge == null) {
			return;
		}

		mBlockedSides[GetEdgeDirection(edge)]++;
	}

	public void OnTriggerExit2D(Collider2D other) {
		EdgeTile edge = other.GetComponent<EdgeTile>();
		if(edge == null) {
			return;
		}
		
		mBlockedSides[GetEdgeDirection(edge)]--;
	}
}
