﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MissionDeck {

	private List<List<Mission>> mDecks = new List<List<Mission>>();

	public MissionDeck() {
		// time, minThreat, maxThreat, difficulty, reward
		List<Mission> safeDeck = new List<Mission>();
		safeDeck.Add(new Mission("tinyConvoy", 2, 0, 25, 6, 1f, "TINY CONVOY"));
		safeDeck.Add(new Mission("secludedWorkshop", 2, 10, 30, 3, 1.05f, "SECLUDED WORKSHOP"));
		safeDeck.Add(new Mission("missionHamlet", 2, 15, 35, 12, 1.1f, "HAMLET"));
		safeDeck.Add(new Mission("tinyManufactory", 3, 10, 30, 3, 1.05f, "TINY MANUFACTORY"));
		safeDeck.Add(new Mission("village", 3, 15, 40, 12, 1.1f, "VILLAGE"));
		safeDeck.Add(new Mission("mediumConvoy", 4, 0, 40, 6, 1f, "MEDIUM CONVOY"));
		safeDeck.Add(new Mission("rundownFactory", 4, 5, 30, 3, 1.05f, "RUNDOWN FACTORY"));
		safeDeck.Add(new Mission("missionSlum", 5, 0, 0, 0, 1f, "SLUM"));
		mDecks.Add(safeDeck);

		List<Mission> riskyDeck = new List<Mission>();
		riskyDeck.Add(new Mission("mercenaryConvoy", 2, 5, 20, 25, 1.1f, "MERCENARY CONVOY"));
		riskyDeck.Add(new Mission("missionFortifiedVillage", 4, 25, 50, 34, 1.3f, "FORTIFIED VILLAGE"));
		riskyDeck.Add(new Mission("vipVilla", 4, 10, 100, 31, 1.5f, "VIP VILLA"));
		riskyDeck.Add(new Mission("supplyDepot", 4, 30, 70, 19, 1.4f, "SUPPLY DEPOT"));
		riskyDeck.Add(new Mission("armory", 3, 0, 0, 50, 1.7f, "ARMORY"));
		riskyDeck.Add(new Mission("seatOfGovernment", 2, 100, 100, 50, 3.0f, "SEAT OF GOVERNMENT"));
		riskyDeck.Add(new Mission("scavengerOutpost", 1, 5, 15, 22, 1.05f, "SCAVENGER OUTPOST"));
		riskyDeck.Add(new Mission("vipConvoy", 2, -25, 100, 25, 1.6f, "VIP CONVOY"));
		mDecks.Add(riskyDeck);

		List<Mission> defensiveDeck = new List<Mission>();
		defensiveDeck.Add(new Mission("smallPolicePatrol", 2, -30, -20, 5, 0.7f, "SMALL POLICE PATROL"));
		defensiveDeck.Add(new Mission("policePatrol", 2, -50, -35, 19, 0.8f, "POLICE PATROL"));
		defensiveDeck.Add(new Mission("mercenaryPatrol", 2, -40, 40, 19, 1f, "MERCENARY PATROL"));
		defensiveDeck.Add(new Mission("policeOutpost", 3, -60, -45, 22, 0.75f, "POLICE OUTPOST"));
		defensiveDeck.Add(new Mission("mercenaryOutpost", 3, -50, 50, 15, 1f, "MERCENARY OUTPOST"));
		defensiveDeck.Add(new Mission("militaryPatrol", 3, -70, -50, 25, 0.7f, "MILITARY PATROL"));
		defensiveDeck.Add(new Mission("militaryBase", 3, -100, -80, 40, 0.6f, "MILITARY BASE"));
		defensiveDeck.Add(new Mission("militaryConvoy", 3, -100, -20, 22, 0.3f, "MILITARY CONVOY"));
		mDecks.Add(defensiveDeck);
	}

	private static MissionDeck instance = null;
	public static MissionDeck Get() {
		if (instance == null) {
			instance = new MissionDeck();
		}
		return instance;
	}

	public Mission GetRandomCard(MissionType type) {
		List<Mission> deck = mDecks[(int)type];
		return deck[Random.Range(0, deck.Count)];
	}
}
