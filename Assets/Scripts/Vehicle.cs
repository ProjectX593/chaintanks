﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class VehicleData {
	public int turretIndex;
	public int upgradeIndex;
	public int health;

	public VehicleData(int turretInd, int upgradeInd, int pHealth) {
		turretIndex = turretInd;
		upgradeIndex = upgradeInd;
		health = pHealth;
	}
}

public class Vehicle : MonoBehaviour {

	public static int MAX_TAILS = 5;
	public static int MAX_UPGRADE_TIER = 6;

	public int maxHealth;
	public bool isPlayerOwned;
	public GameObject upgradedVehicle;
	public int percentUpgradeChance;
	public GameObject turret1;
	public GameObject turret2;
	public GameObject turret3;
	public List<AudioClip> deathSounds;
	public AudioClip collisionSound;
	public Sprite colorblindSprite;

	protected int MAX_HINGE_ANGLE = 30;
	private float TAIL_DISTANCE = 0.85f;
	private float FOLLOW_POINT_MIN_DIST = 0.01f;
	private float FOLLOW_POINT_MAX_DIST = 1.5f;
	private float RED_FLASH_DECAY_RATE = 1.5f;
	private float STOPPING_RATE = 2.0f;
	protected GameObject mHealthBar;

	public Vehicle mTail = null;
	public Vehicle mParent = null;
	public List<Turret> mTurrets = new List<Turret>();
	protected int mHealth = 0;
    protected Globals mGlobals;
	protected SoundManager mSoundManager;
	protected Player mPlayer;
	private int mBatchedTurretUpgrades = 0;
	private float mRedTint = 0;
	protected int mUpgradeIndex = 0;
	protected List<Vector2> mFollowPoints = new List<Vector2>();
	private List<float> mFollowPointDistances = new List<float>(); // the entry at [0] is the distance from [0] to [1] in mFollowPoints
	private float mLastSpeed = 0;
	private bool mDisabled = false;
	private int mStoredTurretIndex = -1;    // will be >= 0 when the vehicle was loaded with vehicle data, meaning it's a player vehicle. Needed to save out new VehicleData. Could be refactored later.
	private Sprite mOriginalSprite;

	private float mFollowCurveLength;
	

	protected virtual void Start() {
        mGlobals = GameObject.Find("Globals").GetComponent<Globals>();
		mSoundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
		mPlayer = GameObject.Find("Player1").GetComponent<Player>();
		mHealth = mHealth == 0 ? GetMaxHealth() : mHealth;
		InitHealthBar();
		GenerateFollowPoints();

		// Vehicles can get upgraded before they start. If so, don't override this.
		if (mOriginalSprite == null) {
			mOriginalSprite = GetComponent<SpriteRenderer>().sprite;
		}
		UpdateColorblindSprite();
	}
	
	protected void InitHealthBar() {
		if (mHealthBar == null) {
			mHealthBar = Instantiate(mGlobals.healthBar, new Vector3(0, 0.5f), Quaternion.identity) as GameObject;
		}
		mHealthBar.GetComponent<HealthBar>().SetVisible(mHealth < GetMaxHealth());
		UpdateHealthBar();
	}

	protected virtual void FixedUpdate() {
		UpdateFollowPoints();
		FollowParent();
	}

	public void GenerateFollowPoints() {
		if (mFollowPoints.Count > 0) {
			return;
		}
		
		Vector2 tailVec = new Vector2(0, -GetTailDistance());
		tailVec = tailVec.Rotate(GetComponent<Rigidbody2D>().rotation);
		tailVec.Normalize();
		mFollowCurveLength = 0.0f;
		for (float i = 0.0f; i < GetMaxFollowDistance(); i += 0.05f) {
			mFollowPoints.Add(new Vector2(transform.position.x, transform.position.y) + tailVec * i);
			mFollowCurveLength += 0.05f;
			mFollowPointDistances.Add(0.05f);
		}
		mFollowPointDistances.RemoveAt(0);  // there's entry corresponding to the last follow point, but they're all the same so lets be lazy and remove the first one...
	}

	protected virtual float GetMaxFollowDistance() {
		return FOLLOW_POINT_MAX_DIST;
	}

	protected virtual float GetTailDistance() {
		return TAIL_DISTANCE;
	}

	protected void UpdateFollowPoints() {
		Vector2 pos = new Vector2(transform.position.x, transform.position.y);
		float dist = (pos - mFollowPoints[0]).magnitude;
		if (dist < FOLLOW_POINT_MIN_DIST) {
			return;
		}
		mFollowPoints.Insert(0, pos);
		mFollowPointDistances.Insert(0, dist);

		if(mFollowPoints.Count <= 1) {  // should really never happen
			print("WARNING: Updated follow points but only found one!");
			return;
		}
		mFollowCurveLength += (pos - mFollowPoints[1]).magnitude;

		float lastLength = (mFollowPoints[mFollowPoints.Count - 1] - mFollowPoints[mFollowPoints.Count - 2]).magnitude;
		while(mFollowCurveLength - lastLength >= GetMaxFollowDistance()) {
			mFollowPoints.RemoveAt(mFollowPoints.Count - 1);
			mFollowCurveLength -= mFollowPointDistances[mFollowPointDistances.Count - 1];
			mFollowPointDistances.RemoveAt(mFollowPointDistances.Count - 1);
			lastLength = (mFollowPoints[mFollowPoints.Count - 1] - mFollowPoints[mFollowPoints.Count - 2]).magnitude;
		}
	}

	public void FollowParent() {
		if(mParent == null || mParent.mFollowPoints.Count == 0 || mParent.mFollowPointDistances.Count == 0) {
			if(mLastSpeed > 0) {
				Vector2 forward = new Vector2(0, mLastSpeed * Time.deltaTime);
				forward = forward.Rotate(GetComponent<Rigidbody2D>().rotation);
				transform.position = transform.position + (new Vector3(forward.x, forward.y));
				mLastSpeed -= Time.deltaTime * STOPPING_RATE;
			}
			return;
		}
		
		float totalDist = 0;
		int ind = 0;
		do {
			totalDist += mParent.mFollowPointDistances[ind];
			ind++;
		} while (totalDist < GetTailDistance() && ind < mParent.mFollowPointDistances.Count);

		if(ind == 0) {
			print("ERROR: Follow error!");
			return;
		}

		Vector2 facing = mParent.mFollowPoints[ind - 1] - mParent.mFollowPoints[ind];
		facing.Normalize();
		Vector2 pos = mParent.mFollowPoints[ind] + facing * Mathf.Max(Mathf.Abs(GetTailDistance() - totalDist), 0.01f);
		Vector2 oldPos = transform.position;
		transform.position = new Vector3(pos.x, pos.y, 0);
		transform.rotation = facing.Rotate(-90).GetAngle();

		mLastSpeed = (pos - oldPos).magnitude / Time.deltaTime;
	}

	protected virtual void Update() {
		if (mHealthBar != null) {
			mHealthBar.transform.position = new Vector3(transform.position.x, transform.position.y + GetHealthBarOffset());
		}

		//TODO: See if this can be removed, seems hacky
		if(mBatchedTurretUpgrades > 0 && mTurrets.Count > 0) {
			mBatchedTurretUpgrades--;
			for(int i = 0; i < mTurrets.Count; i++) {
				mTurrets[i].Upgrade();
			}
		}

		if (mRedTint > 0) {
			mRedTint = Mathf.Max(0.0f, mRedTint - RED_FLASH_DECAY_RATE * Time.deltaTime);
			Color c = new Color(1.0f, 1.0f - mRedTint, 1.0f - mRedTint);
            GetComponent<SpriteRenderer>().material.color = c;
			for(int i = 0; i < mTurrets.Count; i++) {
				mTurrets[i].GetComponent<SpriteRenderer>().material.color = c;
			}
		}
	}

	protected virtual float GetHealthBarOffset() {
		return 0.5f;
	}

	protected virtual void OnTriggerEnter2D(Collider2D other) {
		Vehicle v = other.GetComponent<Vehicle>();
		// Don't apply damage to adjacent vehicles, don't let structures apply damage, don't allow friendly collision damage
		if (v == null || v == mTail || v == mParent || ProgressionManager.Get().missionComplete || GetComponent<Structure>() != null || (isPlayerOwned == v.isPlayerOwned)) {
			return;
		}

		v.TakeDamage(mUpgradeIndex / 2 + 1);
		mSoundManager.PlaySound(collisionSound, (mPlayer.transform.position - transform.position).magnitude, true);
	}


	public virtual void AddRandomTurret() {
		if(turret1 == null && turret2 == null && turret3 == null) {
			Debug.LogError("ERROR: Turret not found when trying to add random turret!");
			return;
		}

        float r = Random.value;
        GameObject turret = null;
		
		if (r < 0.3333) {
            turret = turret1;
        } else if (r < 0.6666) {
            turret = turret2;
		} else {
            turret = turret3;
		}

		AddTurret(turret);
    }

	public void AddTurretWithIndex(int turretIndex) {
		GameObject turret = null;
		if (turretIndex == 0) {
			turret = turret1;
		} else if(turretIndex == 1) {
			turret = turret2;
		} else if(turretIndex == 2) {
			turret = turret3;
		}

		if (turret) {
			AddTurret(turret);
		}
	}

	protected virtual void AddTurret(GameObject turretToInstantiate) {
		Turret turret = (Instantiate(turretToInstantiate, transform.position, transform.rotation) as GameObject).GetComponent<Turret>();
		turret.transform.parent = transform;
		turret.SetPlayerOwned(isPlayerOwned);
		mTurrets.Add(turret);
	}

	public virtual void AddTail() {
		if (mTail != null) {
			Debug.Log("ERROR: TRIED TO ADD A TAIL TO A VEHICLE THAT ALREADY HAD A TAIL.");
			return;
		}

		if (mGlobals == null) {
			mGlobals = GameObject.Find("Globals").GetComponent<Globals>();  //I DONT EVEN CARE
		}

		mTail = GenerateTail();
		mTail.Heal(mTail.maxHealth);

		if (mTail != null) {
			mTail.SetParent(this);
			GenerateFollowPoints(); // this is important for newly spawned enemies - their tails don't have follow points yet
			mTail.FollowParent();
		}
	}

	protected virtual Vehicle GenerateTail() {
		Vehicle current = this;
		while(current != null) {
			Truck t = current.GetComponent<Truck>();
			if (t != null) {
				return (Instantiate(t.tailToAdd) as GameObject).GetComponent<Vehicle>();
			}
			current = current.mParent;
		}

		Debug.Log("ERROR: Could not find head from tail trying to generate new tail!");
		return null;
	}

	public void SetParent(Vehicle v) {
		mParent = v;
	}

	protected void UpdateHealthBar() {
		if (mHealthBar) {
			mHealthBar.GetComponent<HealthBar>().SetHealthPercent((float)mHealth / GetMaxHealth());
		}
	}

	public virtual void TakeDamage(int damage) {
		if (mHealthBar != null && (mTurrets.Count > 0 || GetComponent<Structure>() == null)) {
			mHealthBar.GetComponent<HealthBar>().SetVisible(true);
		}
		mHealth -= damage;
		UpdateHealthBar();
		mRedTint = 1.0f;
		if(mHealth <= 0) {
            OnDie();
		}
    }

	// returns true if any damage was healed
	public int Heal(int damage) {
		if(mHealth == GetMaxHealth()) {
			return 0;
		}

		int healed = Mathf.Min(damage, GetMaxHealth() - mHealth);
		mHealth += healed;

		UpdateHealthBar();
		if (mHealthBar != null && mHealth == GetMaxHealth()) {
			mHealthBar.GetComponent<HealthBar>().SetVisible(false);
		}

		return healed;
	}

	protected void Explode() {
		GameObject explosion = Instantiate(mGlobals.explosion, transform.position, Quaternion.identity) as GameObject;
		Vector3 size = GetComponent<SpriteRenderer>().bounds.size;
		explosion.GetComponent<Explosion>().SetWidth(Mathf.Max(size.x, size.y) * 1.5f);
	}

    protected virtual void OnDie() {
		mSoundManager.PlayRandomSound(deathSounds, (transform.position - mPlayer.transform.position).magnitude);

		if (mParent != null) {
			mParent.mTail = null;
		}
		if (mTail != null) {
			mTail.mParent = null;
		}

		Vehicle current = mTail;
		while(current != null) {
			current.SetDisabled(true);
			for (int i = 0; i < current.mTurrets.Count; i++) {
				current.mTurrets[i].SetFiring(false);
			}
			current = current.mTail;
		}

		if (isPlayerOwned) {
			mPlayer.UpdateSpeed();
		}

		Explode();
		Destroy(mHealthBar);
		Destroy(gameObject);
	}

    public virtual void Upgrade() {
		if(upgradedVehicle == null) {
			print("WARNING: TRIED TO UPGRADE UNUPGRADEABLE VEHICLE");
			return;
		}

		int missingHealth = GetMaxHealth() - mHealth;
        maxHealth = upgradedVehicle.GetComponent<Vehicle>().maxHealth;
        mHealth = GetMaxHealth() - missingHealth;
		percentUpgradeChance = upgradedVehicle.GetComponent<Vehicle>().percentUpgradeChance;
		colorblindSprite = upgradedVehicle.GetComponent<Vehicle>().colorblindSprite;
		mOriginalSprite = upgradedVehicle.GetComponent<SpriteRenderer>().sprite;
		upgradedVehicle = upgradedVehicle.GetComponent<Vehicle>().upgradedVehicle;
		UpdateColorblindSprite();

		if (mTurrets.Count > 0) {
			for (int i = 0; i < mTurrets.Count; i++) {
				mTurrets[i].Upgrade();
			}
		} else {
			mBatchedTurretUpgrades++;
		}
		mUpgradeIndex++;
    }

	public void SetDisabled(bool disabled) {
		mDisabled = disabled;
		
		if(disabled && isPlayerOwned && PlayerPrefs.GetString("reconnectTutorialShown", "false") == "false") {
			PlayerPrefs.SetString("reconnectTutorialShown", "true");
			GameObject.Find("HUD").GetComponent<HUD>().ToggleReconnectTutorial();
		}
	}

	// Note that VehicleData represents a vehicle with a single turret, thus exactly one turret will be added
	public VehicleData GetData() {
		return new VehicleData(mStoredTurretIndex, mUpgradeIndex, mHealth);
	}

	public void UpdateColorblindSprite() {
		SpriteRenderer sr = GetComponent<SpriteRenderer>();
		if (Options.colorblindEnabled && colorblindSprite != null) {
			sr.sprite = colorblindSprite;
		} else if(mOriginalSprite != null){
			sr.sprite = mOriginalSprite;
		}

		for(int i = 0; i < mTurrets.Count; i++) {
			mTurrets[i].UpdateColorblindSprite();
		}
	}

	public void InitFromData(VehicleData data) {
		if (mTurrets.Count > 0) {
			Debug.LogError("ERROR: Tried to init vehicle with VehicleData, but it already has a turret!");
			return;
		}

		AddTurretWithIndex(data.turretIndex);
		mStoredTurretIndex = data.turretIndex;
		for(int i = 0; i < data.upgradeIndex; i++) {
			Upgrade();
		}
		mHealth = data.health;
	}

	public virtual int GetMaxHealth() {
		if (isPlayerOwned) {
			return (int)(maxHealth * ProgressionManager.Get().GetTechBonusPercent(TechType.hp) * ProgressionManager.Get().GetDifficultyHealthMultiplier());
		} else {
			return maxHealth;
		}
	}

	public int health {
		get {
			return mHealth;
		}
	}

	public int upgradeTier {
		get {
			return mUpgradeIndex;
		}
	}

	public virtual bool disabled {
		get {
			return mDisabled;
		}
	}
}
