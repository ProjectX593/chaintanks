﻿// Tech doesn't do much by itself, this class mainly just holds an enum and associates config data to it

public enum TechType {
	hp,
	rateOfFire,
	speed,
	bulletSpeed,
	techTypeCount
}

public class Tech {

	public static readonly int[] techCosts = new int[5]{0, 30, 120, 250, 500};

	public static float GetBonusForType(TechType type) {
		switch (type) {
			case TechType.hp:
				return 0.08f;
			case TechType.rateOfFire:
				return 0.08f;
			case TechType.speed:
				return 0.06f;
			case TechType.bulletSpeed:
				return 0.08f;
			default:
				return 0f;
		}
	}

	public static string GetPostfixForType(TechType type) {
		switch (type) {
			case TechType.hp:
				return "HP";
			case TechType.rateOfFire:
				return "ROF";
			case TechType.speed:
				return "SPD";
			case TechType.bulletSpeed:
				return "BSPD";
			default:
				return "";
		}
	}
}
