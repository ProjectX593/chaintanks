﻿using UnityEngine;
using System.Collections;

public class RandomRotation : MonoBehaviour {
	
	void Start () {
		float r = Random.value;
		if(r < 0.25) {
			transform.rotation = Quaternion.AngleAxis(0, Vector3.forward);
		}else if (r < 0.5) {
			transform.rotation = Quaternion.AngleAxis(90, Vector3.forward);
		} else if (r < 0.75) {
			transform.rotation = Quaternion.AngleAxis(180, Vector3.forward);
		} else  {
			transform.rotation = Quaternion.AngleAxis(270, Vector3.forward);
		}
	}
}
