﻿using UnityEngine;
using System.Collections.Generic;

public class Bullet : MonoBehaviour {
	
	public int damage;
	public float speed;
	public List<AudioClip> hitSounds;
	public GameObject impact;
	
	private bool mIsPlayerOwned = true;
	private Rigidbody2D mRigidBody;
	private SoundManager mSoundManager;

	void Start() {
		mRigidBody = GetComponent<Rigidbody2D>();
	}

	void FixedUpdate() {
		Vector2 move = (new Vector2(0, GetSpeed() * Time.deltaTime)).Rotate(mRigidBody.rotation);
		transform.position += new Vector3(move.x, move.y, 0);
	}

	void OnTriggerEnter2D(Collider2D other) {
		bool hit = false;
		
		if(other.GetComponent<EdgeTile>() != null) {
			hit = true;
		}

		ProgressionManager pm = ProgressionManager.Get();
		Vehicle v = other.GetComponent<Vehicle>();
		if(!pm.missionComplete && v != null && (mIsPlayerOwned != v.isPlayerOwned || v.disabled) && v.health > 0) {
			mSoundManager.PlayRandomSound(hitSounds, (transform.position - GameObject.Find("Player1").transform.position).magnitude, true);
			v.TakeDamage(damage);
			hit = true;
		}

		if (hit) {
			Instantiate(impact, transform.position, Quaternion.identity);
			Destroy(gameObject);
		}
	}

	public void SetSoundManager(SoundManager soundManager) {
		mSoundManager = soundManager;
	}

	public void SetPlayerOwned(bool playerOwned) {
		mIsPlayerOwned = playerOwned;
	}

	public float GetSpeed() {
		if (mIsPlayerOwned) {
			return speed * ProgressionManager.Get().GetTechBonusPercent(TechType.bulletSpeed);
		} else {
			return speed;
		}
	}
}
