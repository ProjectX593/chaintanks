﻿using UnityEngine;
using System.Collections;

public class Escort : Enemy {

	public GameObject escortTail;

	// Use this for initialization
	protected override void Start() {
		mBufferedStrength = 0;
		base.Start();

		int tails = Random.Range(2, 5);
		for (int i = 0; i < tails; i++) {
			AddTail();
		}

		int upgradeLevel = (int)ProgressionManager.Get().GetDifficultyValue(0, 6);
		for (int i = 0; i < upgradeLevel; i++) {
			Upgrade();
		}
	}

	public override void AddTail() {
		Vehicle current = this;
		while(current.mTail != null) {
			current = current.mTail;
		}
		
		GameObject test = Instantiate(escortTail, transform.position, transform.rotation) as GameObject;
		Vehicle test2 = test.GetComponent<Vehicle>();
		current.mTail = test2;
		current.mTail.SetParent(current);
		current.GenerateFollowPoints(); // this is important for newly spawned enemies - their tails don't have follow points yet
		current.mTail.FollowParent();
	}

	public override void AwardStrength() {
		return;
	}
}